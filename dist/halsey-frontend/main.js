(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_desktop_img_desktop_img_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/desktop-img/desktop-img.component */ "./src/app/components/desktop-img/desktop-img.component.ts");
/* harmony import */ var _components_phone_img_phone_img_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/phone-img/phone-img.component */ "./src/app/components/phone-img/phone-img.component.ts");
/* harmony import */ var _components_smallphone_img_smallphone_img_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/smallphone-img/smallphone-img.component */ "./src/app/components/smallphone-img/smallphone-img.component.ts");
/* harmony import */ var _components_contacto_contacto_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/contacto/contacto.component */ "./src/app/components/contacto/contacto.component.ts");
/* harmony import */ var _components_plataforma_pago_plataforma_pago_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/plataforma-pago/plataforma-pago.component */ "./src/app/components/plataforma-pago/plataforma-pago.component.ts");
/* harmony import */ var _components_exclusivo_exclusivo_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/exclusivo/exclusivo.component */ "./src/app/components/exclusivo/exclusivo.component.ts");











const routes = [
    { path: "", component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"] },
    { path: "wallpers-escritorio", component: _components_desktop_img_desktop_img_component__WEBPACK_IMPORTED_MODULE_3__["DesktopImgComponent"] },
    { path: "wallpers-telefono", component: _components_phone_img_phone_img_component__WEBPACK_IMPORTED_MODULE_4__["PhoneImgComponent"] },
    { path: "wallpers-telefono-pequeno", component: _components_smallphone_img_smallphone_img_component__WEBPACK_IMPORTED_MODULE_5__["SmallphoneImgComponent"] },
    { path: "contacto", component: _components_contacto_contacto_component__WEBPACK_IMPORTED_MODULE_6__["ContactoComponent"] },
    { path: "cabina-pago", component: _components_plataforma_pago_plataforma_pago_component__WEBPACK_IMPORTED_MODULE_7__["PlataformaPagoComponent"] },
    { path: "exclusivo", component: _components_exclusivo_exclusivo_component__WEBPACK_IMPORTED_MODULE_8__["ExclusivoComponent"] },
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, { useHash: true })], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, { useHash: true })],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-loading-bar/core */ "./node_modules/@ngx-loading-bar/core/__ivy_ngcc__/fesm2015/ngx-loading-bar-core.js");




class AppComponent {
    constructor(router) {
        this.router = router;
        this.title = 'halsey-frontend';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"])); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 2, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "ngx-loading-bar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "router-outlet");
    } }, directives: [_ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_2__["LoadingBarComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }]; }, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_desktop_img_desktop_img_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/desktop-img/desktop-img.component */ "./src/app/components/desktop-img/desktop-img.component.ts");
/* harmony import */ var _components_phone_img_phone_img_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/phone-img/phone-img.component */ "./src/app/components/phone-img/phone-img.component.ts");
/* harmony import */ var _components_smallphone_img_smallphone_img_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/smallphone-img/smallphone-img.component */ "./src/app/components/smallphone-img/smallphone-img.component.ts");
/* harmony import */ var _components_contacto_contacto_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/contacto/contacto.component */ "./src/app/components/contacto/contacto.component.ts");
/* harmony import */ var _components_plataforma_pago_plataforma_pago_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/plataforma-pago/plataforma-pago.component */ "./src/app/components/plataforma-pago/plataforma-pago.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/animations.js");
/* harmony import */ var _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/modal/modal.component */ "./src/app/components/modal/modal.component.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _components_modal_descarga_descarga_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/modal/descarga/descarga.component */ "./src/app/components/modal/descarga/descarga.component.ts");
/* harmony import */ var ngx_paypal__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-paypal */ "./node_modules/ngx-paypal/__ivy_ngcc__/fesm2015/ngx-paypal.js");
/* harmony import */ var _components_plataforma_pago_modal_pago_modal_pago_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/plataforma-pago/modal-pago/modal-pago.component */ "./src/app/components/plataforma-pago/modal-pago/modal-pago.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ngx-loading-bar/core */ "./node_modules/@ngx-loading-bar/core/__ivy_ngcc__/fesm2015/ngx-loading-bar-core.js");
/* harmony import */ var _components_exclusivo_exclusivo_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/exclusivo/exclusivo.component */ "./src/app/components/exclusivo/exclusivo.component.ts");























class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [_components_modal_modal_component__WEBPACK_IMPORTED_MODULE_11__["ModalComponent"]], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__["MatFormFieldModule"],
            _angular_material_button__WEBPACK_IMPORTED_MODULE_19__["MatButtonModule"],
            _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_20__["LoadingBarModule"],
            _angular_material_input__WEBPACK_IMPORTED_MODULE_18__["MatInputModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_16__["ReactiveFormsModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__["BrowserAnimationsModule"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__["MatDialogModule"],
            ngx_paypal__WEBPACK_IMPORTED_MODULE_14__["NgxPayPalModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
        _components_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"],
        _components_desktop_img_desktop_img_component__WEBPACK_IMPORTED_MODULE_5__["DesktopImgComponent"],
        _components_phone_img_phone_img_component__WEBPACK_IMPORTED_MODULE_6__["PhoneImgComponent"],
        _components_smallphone_img_smallphone_img_component__WEBPACK_IMPORTED_MODULE_7__["SmallphoneImgComponent"],
        _components_contacto_contacto_component__WEBPACK_IMPORTED_MODULE_8__["ContactoComponent"],
        _components_plataforma_pago_plataforma_pago_component__WEBPACK_IMPORTED_MODULE_9__["PlataformaPagoComponent"],
        _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_11__["ModalComponent"],
        _components_modal_descarga_descarga_component__WEBPACK_IMPORTED_MODULE_13__["DescargaComponent"],
        _components_plataforma_pago_modal_pago_modal_pago_component__WEBPACK_IMPORTED_MODULE_15__["ModalPagoComponent"],
        _components_exclusivo_exclusivo_component__WEBPACK_IMPORTED_MODULE_21__["ExclusivoComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__["MatFormFieldModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_19__["MatButtonModule"],
        _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_20__["LoadingBarModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_18__["MatInputModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_16__["ReactiveFormsModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__["BrowserAnimationsModule"],
        _angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__["MatDialogModule"],
        ngx_paypal__WEBPACK_IMPORTED_MODULE_14__["NgxPayPalModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                    _components_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"],
                    _components_desktop_img_desktop_img_component__WEBPACK_IMPORTED_MODULE_5__["DesktopImgComponent"],
                    _components_phone_img_phone_img_component__WEBPACK_IMPORTED_MODULE_6__["PhoneImgComponent"],
                    _components_smallphone_img_smallphone_img_component__WEBPACK_IMPORTED_MODULE_7__["SmallphoneImgComponent"],
                    _components_contacto_contacto_component__WEBPACK_IMPORTED_MODULE_8__["ContactoComponent"],
                    _components_plataforma_pago_plataforma_pago_component__WEBPACK_IMPORTED_MODULE_9__["PlataformaPagoComponent"],
                    _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_11__["ModalComponent"],
                    _components_modal_descarga_descarga_component__WEBPACK_IMPORTED_MODULE_13__["DescargaComponent"],
                    _components_plataforma_pago_modal_pago_modal_pago_component__WEBPACK_IMPORTED_MODULE_15__["ModalPagoComponent"],
                    _components_exclusivo_exclusivo_component__WEBPACK_IMPORTED_MODULE_21__["ExclusivoComponent"],
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__["MatFormFieldModule"],
                    _angular_material_button__WEBPACK_IMPORTED_MODULE_19__["MatButtonModule"],
                    _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_20__["LoadingBarModule"],
                    _angular_material_input__WEBPACK_IMPORTED_MODULE_18__["MatInputModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_16__["ReactiveFormsModule"],
                    _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__["BrowserAnimationsModule"],
                    _angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__["MatDialogModule"],
                    ngx_paypal__WEBPACK_IMPORTED_MODULE_14__["NgxPayPalModule"]
                ],
                providers: [_components_modal_modal_component__WEBPACK_IMPORTED_MODULE_11__["ModalComponent"]],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/components/contacto/contacto.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/contacto/contacto.component.ts ***!
  \***********************************************************/
/*! exports provided: ContactoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoComponent", function() { return ContactoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_idioma_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/idioma.service */ "./src/app/services/idioma.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-loading-bar/core */ "./node_modules/@ngx-loading-bar/core/__ivy_ngcc__/fesm2015/ngx-loading-bar-core.js");





class ContactoComponent {
    constructor(idiomas, dialog, loadingBarService) {
        this.idiomas = idiomas;
        this.dialog = dialog;
        this.loadingBarService = loadingBarService;
        this.background = `assets/contacto/${idiomas.idioma}.jpg`;
    }
    ngOnInit() {
    }
    doSomething() {
        // const dialogRef = this.dialog.open(DescargaComponent, {
        //   width: '40%'
        // });
        this.loadingBarService.complete();
    }
}
ContactoComponent.ɵfac = function ContactoComponent_Factory(t) { return new (t || ContactoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_idioma_service__WEBPACK_IMPORTED_MODULE_1__["IdiomasLanding"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__["LoadingBarService"])); };
ContactoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ContactoComponent, selectors: [["app-contacto"]], decls: 1, vars: 1, consts: [[1, "main-home", 3, "src", "load"]], template: function ContactoComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("load", function ContactoComponent_Template_img_load_0_listener() { return ctx.doSomething(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.background, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    } }, styles: [".main-home[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    background-position: center; \r\n    background-repeat: no-repeat; \r\n    background-size: cover; \r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb250YWN0by9jb250YWN0by5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksV0FBVztJQUNYLDJCQUEyQixFQUFFLHFCQUFxQjtJQUNsRCw0QkFBNEIsRUFBRSw0QkFBNEI7SUFDMUQsc0JBQXNCLEVBQUUsOERBQThEO0FBQzFGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jb250YWN0by9jb250YWN0by5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW4taG9tZXtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyOyAvKiBDZW50ZXIgdGhlIGltYWdlICovXHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0OyAvKiBEbyBub3QgcmVwZWF0IHRoZSBpbWFnZSAqL1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjsgLyogUmVzaXplIHRoZSBiYWNrZ3JvdW5kIGltYWdlIHRvIGNvdmVyIHRoZSBlbnRpcmUgY29udGFpbmVyICovXHJcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContactoComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-contacto',
                templateUrl: './contacto.component.html',
                styleUrls: ['./contacto.component.css']
            }]
    }], function () { return [{ type: _services_idioma_service__WEBPACK_IMPORTED_MODULE_1__["IdiomasLanding"] }, { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] }, { type: _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__["LoadingBarService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/components/desktop-img/desktop-img.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/desktop-img/desktop-img.component.ts ***!
  \*****************************************************************/
/*! exports provided: DesktopImgComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DesktopImgComponent", function() { return DesktopImgComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _modal_modal_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../modal/modal.component */ "./src/app/components/modal/modal.component.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-loading-bar/core */ "./node_modules/@ngx-loading-bar/core/__ivy_ngcc__/fesm2015/ngx-loading-bar-core.js");





const _c0 = ["init"];
class DesktopImgComponent {
    constructor(dialog, loadingBarService) {
        this.dialog = dialog;
        this.loadingBarService = loadingBarService;
        const muestraWidth = () => {
            setTimeout(() => {
                const x = document.getElementById("background-img").getBoundingClientRect().width;
                console.log(x);
                const elements = ['p1', 'p2', 'p3'];
                const elements2 = ['p4', 'p5', 'p6'];
                const elements3 = ['p7', 'p8', 'p9'];
                const elements4 = ['p10', 'p11', 'p12'];
                const elements5 = ['p13', 'p14', 'p15'];
                const elements6 = ['p16', 'p17', 'p18'];
                const elements7 = ['p19', 'p20', 'p21'];
                const elements8 = ['p22', 'p23', 'p24'];
                const elements9 = ['p25', 'p26', 'p27'];
                const elements10 = ['p28', 'p29', 'p30'];
                const elements11 = ['p31', 'p32', 'p33'];
                const elements12 = ['p34', 'p35', 'p36'];
                const elements13 = ['p37', 'p38', 'p39'];
                const elements14 = ['p40', 'p41', 'p42'];
                const elements15 = ['p43', 'p44', 'p45'];
                for (let i in elements) {
                    const element = document.getElementById(elements[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${28 * x / 923}px`;
                }
                for (let i in elements2) {
                    const element = document.getElementById(elements2[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${166 * x / 923}px`;
                }
                for (let i in elements3) {
                    const element = document.getElementById(elements3[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${307 * x / 923}px`;
                }
                for (let i in elements4) {
                    const element = document.getElementById(elements4[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${475 * x / 923}px`;
                }
                for (let i in elements5) {
                    const element = document.getElementById(elements5[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${613 * x / 923}px`;
                }
                for (let i in elements6) {
                    const element = document.getElementById(elements6[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${760 * x / 923}px`;
                }
                for (let i in elements7) {
                    const element = document.getElementById(elements7[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${928 * x / 923}px`;
                }
                for (let i in elements8) {
                    const element = document.getElementById(elements8[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${1069 * x / 923}px`;
                }
                for (let i in elements9) {
                    const element = document.getElementById(elements9[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${1206 * x / 923}px`;
                }
                for (let i in elements10) {
                    const element = document.getElementById(elements10[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${1371 * x / 923}px`;
                }
                for (let i in elements11) {
                    const element = document.getElementById(elements11[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${1510 * x / 923}px`;
                }
                for (let i in elements12) {
                    const element = document.getElementById(elements12[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${1652 * x / 923}px`;
                }
                for (let i in elements13) {
                    const element = document.getElementById(elements13[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${1824 * x / 923}px`;
                }
                for (let i in elements14) {
                    const element = document.getElementById(elements14[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${1965 * x / 923}px`;
                }
                for (let i in elements15) {
                    const element = document.getElementById(elements15[i]);
                    element.style.height = `${126 * x / 923}px`;
                    element.style.top = `${2102 * x / 923}px`;
                }
                // const element = document.getElementById('desktopImg')
                // element.style.top = `${1000*x/1662}px`
                // const element2 = document.getElementById('phoneImg')
                // element2.style.top = `${1959*x/1662}px`
                // const element3 = document.getElementById('smallphoneImg')
                // element3.style.top = `${2612*x/1662}px`
                // const element4 = document.getElementById('banderas')
                // element4.style.height = `${644*x/1662}px`
                muestraWidth();
            }, 400);
        };
        muestraWidth();
        // this.init.scrollIntoView();
    }
    ngOnInit() {
        setTimeout(() => {
            this.init.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
        }, 500);
    }
    openDialog(link) {
        console.log(link);
        const dialogRef = this.dialog.open(_modal_modal_component__WEBPACK_IMPORTED_MODULE_1__["ModalComponent"], {
            width: '45%',
            data: link,
            position: { top: '10px' }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }
    doSomething() {
        // const dialogRef = this.dialog.open(DescargaComponent, {
        //   width: '40%'
        // });
        this.loadingBarService.complete();
    }
}
DesktopImgComponent.ɵfac = function DesktopImgComponent_Factory(t) { return new (t || DesktopImgComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__["LoadingBarService"])); };
DesktopImgComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: DesktopImgComponent, selectors: [["app-desktop-img"]], viewQuery: function DesktopImgComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
    } if (rf & 2) {
        var _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.init = _t.first);
    } }, decls: 48, vars: 0, consts: [[2, "top", "0"], ["init", ""], ["src", "assets/desktopImg/list.jpg", "id", "background-img", 1, "main-home", 3, "load"], ["id", "p1", 1, "p1", 3, "click"], ["id", "p2", 1, "p2", 3, "click"], ["id", "p3", 1, "p3", 3, "click"], ["id", "p4", 1, "p4", 3, "click"], ["id", "p5", 1, "p5", 3, "click"], ["id", "p6", 1, "p6", 3, "click"], ["id", "p7", 1, "p7", 3, "click"], ["id", "p8", 1, "p8", 3, "click"], ["id", "p9", 1, "p9", 3, "click"], ["id", "p10", 1, "p10", 3, "click"], ["id", "p11", 1, "p11", 3, "click"], ["id", "p12", 1, "p12", 3, "click"], ["id", "p13", 1, "p13", 3, "click"], ["id", "p14", 1, "p14", 3, "click"], ["id", "p15", 1, "p15", 3, "click"], ["id", "p16", 1, "p16", 3, "click"], ["id", "p17", 1, "p17", 3, "click"], ["id", "p18", 1, "p18", 3, "click"], ["id", "p19", 1, "p19", 3, "click"], ["id", "p20", 1, "p20", 3, "click"], ["id", "p21", 1, "p21", 3, "click"], ["id", "p22", 1, "p22", 3, "click"], ["id", "p23", 1, "p23", 3, "click"], ["id", "p24", 1, "p24", 3, "click"], ["id", "p25", 1, "p25", 3, "click"], ["id", "p26", 1, "p26", 3, "click"], ["id", "p27", 1, "p27", 3, "click"], ["id", "p28", 1, "p28", 3, "click"], ["id", "p29", 1, "p29", 3, "click"], ["id", "p30", 1, "p30", 3, "click"], ["id", "p31", 1, "p31", 3, "click"], ["id", "p32", 1, "p32", 3, "click"], ["id", "p33", 1, "p33", 3, "click"], ["id", "p34", 1, "p34", 3, "click"], ["id", "p35", 1, "p35", 3, "click"], ["id", "p36", 1, "p36", 3, "click"], ["id", "p37", 1, "p37", 3, "click"], ["id", "p38", 1, "p38", 3, "click"], ["id", "p39", 1, "p39", 3, "click"], ["id", "p40", 1, "p40", 3, "click"], ["id", "p41", 1, "p41", 3, "click"], ["id", "p42", 1, "p42", 3, "click"], ["id", "p43", 1, "p43", 3, "click"], ["id", "p44", 1, "p44", 3, "click"], ["id", "p45", 1, "p45", 3, "click"]], template: function DesktopImgComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 0, 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("load", function DesktopImgComponent_Template_img_load_2_listener() { return ctx.doSomething(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_3_listener() { return ctx.openDialog("assets/desktopImg/1P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_4_listener() { return ctx.openDialog("assets/desktopImg/2P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_5_listener() { return ctx.openDialog("assets/desktopImg/3P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_6_listener() { return ctx.openDialog("assets/desktopImg/4P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_7_listener() { return ctx.openDialog("assets/desktopImg/5P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_8_listener() { return ctx.openDialog("assets/desktopImg/6P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_9_listener() { return ctx.openDialog("assets/desktopImg/7P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_10_listener() { return ctx.openDialog("assets/desktopImg/8P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_11_listener() { return ctx.openDialog("assets/desktopImg/9P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_12_listener() { return ctx.openDialog("assets/desktopImg/10P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_13_listener() { return ctx.openDialog("assets/desktopImg/11P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_14_listener() { return ctx.openDialog("assets/desktopImg/12P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_15_listener() { return ctx.openDialog("assets/desktopImg/13P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "button", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_16_listener() { return ctx.openDialog("assets/desktopImg/14P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_17_listener() { return ctx.openDialog("assets/desktopImg/15P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_18_listener() { return ctx.openDialog("assets/desktopImg/16P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "button", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_19_listener() { return ctx.openDialog("assets/desktopImg/17P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_20_listener() { return ctx.openDialog("assets/desktopImg/18P.JPG"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_21_listener() { return ctx.openDialog("assets/desktopImg/19P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_22_listener() { return ctx.openDialog("assets/desktopImg/20P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "button", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_23_listener() { return ctx.openDialog("assets/desktopImg/21P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "button", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_24_listener() { return ctx.openDialog("assets/desktopImg/22P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "button", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_25_listener() { return ctx.openDialog("assets/desktopImg/23P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "button", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_26_listener() { return ctx.openDialog("assets/desktopImg/24P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "button", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_27_listener() { return ctx.openDialog("assets/desktopImg/25P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "button", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_28_listener() { return ctx.openDialog("assets/desktopImg/26P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "button", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_29_listener() { return ctx.openDialog("assets/desktopImg/27P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "button", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_30_listener() { return ctx.openDialog("assets/desktopImg/28P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "button", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_31_listener() { return ctx.openDialog("assets/desktopImg/29P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "button", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_32_listener() { return ctx.openDialog("assets/desktopImg/30P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "button", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_33_listener() { return ctx.openDialog("assets/desktopImg/31P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "button", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_34_listener() { return ctx.openDialog("assets/desktopImg/32P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "button", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_35_listener() { return ctx.openDialog("assets/desktopImg/33P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "button", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_36_listener() { return ctx.openDialog("assets/desktopImg/34P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "button", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_37_listener() { return ctx.openDialog("assets/desktopImg/35P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "button", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_38_listener() { return ctx.openDialog("assets/desktopImg/36P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "button", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_39_listener() { return ctx.openDialog("assets/desktopImg/37P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "button", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_40_listener() { return ctx.openDialog("assets/desktopImg/38P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "button", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_41_listener() { return ctx.openDialog("assets/desktopImg/39P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "button", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_42_listener() { return ctx.openDialog("assets/desktopImg/40P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "button", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_43_listener() { return ctx.openDialog("assets/desktopImg/41P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "button", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_44_listener() { return ctx.openDialog("assets/desktopImg/42P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "button", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_45_listener() { return ctx.openDialog("assets/desktopImg/43P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "button", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_46_listener() { return ctx.openDialog("assets/desktopImg/44P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "button", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DesktopImgComponent_Template_button_click_47_listener() { return ctx.openDialog("assets/desktopImg/45P.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: [".main-home[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    \r\n    background-position: center; \r\n    background-repeat: no-repeat; \r\n    background-size: cover; \r\n}\r\n.p1[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 67.5%;\r\n    top: 28px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p2[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 126px;\r\n    right: 41%;\r\n    top: 28px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p3[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 14.5%;\r\n    top: 28px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p4[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 64.5%;\r\n    top: 363px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p5[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 38.5%;\r\n    top: 363px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p6[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 11.5%;\r\n    top: 363px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p7[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 67.5%;\r\n    top: 666px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p8[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 40.5%;\r\n    top: 666px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p9[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 14.5%;\r\n    top: 666px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p10[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 64.5%;\r\n    top: 1035px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p11[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 38.5%;\r\n    top: 1035px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p12[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 11.5%;\r\n    top: 1035px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p13[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 67.5%;\r\n    top: 1342px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p14[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent ;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 40.5% ;\r\n    top: 1342px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p15[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 14.5%;\r\n    top: 1342px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p16[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 64.5%;\r\n    top: 1655px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p17[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 38.5%;\r\n    top: 1655px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p18[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right:11.5%;\r\n    top: 1655px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p19[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right:  67.5%;\r\n    top: 2025px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p20[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 40.5%;\r\n    top: 2025px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p21[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 14.5%;\r\n    top: 2025px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p22[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 63.5%;\r\n    top: 2330px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p23[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 37.5%;\r\n    top: 2330px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p24[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right:11.5%;\r\n    top: 2330px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p25[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 67.5%;\r\n    top: 2635px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p26[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 41.5%;\r\n    top: 2635px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p27[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 14.5%;\r\n    top: 2635px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p28[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right:64.5%;\r\n    top: 2992px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p29[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 37.5%;\r\n    top: 2992px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p30[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 11.5%;\r\n    top: 2992px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p31[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 67.5%;\r\n    top: 3299px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p32[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 40.5%;\r\n    top: 3299px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p33[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 14.5%;\r\n    top: 3299px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p34[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right:64.5%;\r\n    top: 3604px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p35[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 37.5%;\r\n    top: 3604px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p36[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 11.5%;\r\n    top: 3604px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p37[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 67.5%;\r\n    top: 3978px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p38[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 40.5%;\r\n    top: 3978px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p39[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 14.5%;\r\n    top: 3978px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p40[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 64.5%;\r\n    top: 4283px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p41[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 37.5%;\r\n    top: 4283px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p42[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 11.5%;\r\n    top: 4283px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p43[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 67.5%;\r\n    top: 4587px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p44[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 40.5%;\r\n    top: 4587px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p45[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 24.5%;\r\n    height: 20%;\r\n    right: 14.5%;\r\n    top: 4587px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9kZXNrdG9wLWltZy9kZXNrdG9wLWltZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksV0FBVztJQUNYLG9CQUFvQjtJQUNwQiwyQkFBMkIsRUFBRSxxQkFBcUI7SUFDbEQsNEJBQTRCLEVBQUUsNEJBQTRCO0lBQzFELHNCQUFzQixFQUFFLDhEQUE4RDtBQUMxRjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixTQUFTOztJQUVULG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFVBQVU7SUFDVixTQUFTOztJQUVULG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixTQUFTOztJQUVULG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDhCQUE4QjtJQUM5QixZQUFZO0lBQ1osV0FBVztJQUNYLGFBQWE7SUFDYixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFdBQVc7SUFDWCxXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLGFBQWE7SUFDYixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFdBQVc7SUFDWCxXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFdBQVc7SUFDWCxXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFdBQVc7SUFDWCxXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGVza3RvcC1pbWcvZGVza3RvcC1pbWcuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluLWhvbWV7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIC8qIGhlaWdodDogNTAwMHB4OyAqL1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyOyAvKiBDZW50ZXIgdGhlIGltYWdlICovXHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0OyAvKiBEbyBub3QgcmVwZWF0IHRoZSBpbWFnZSAqL1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjsgLyogUmVzaXplIHRoZSBiYWNrZ3JvdW5kIGltYWdlIHRvIGNvdmVyIHRoZSBlbnRpcmUgY29udGFpbmVyICovXHJcbn1cclxuLnAxe1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiA2Ny41JTtcclxuICAgIHRvcDogMjhweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDEyNnB4O1xyXG4gICAgcmlnaHQ6IDQxJTtcclxuICAgIHRvcDogMjhweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAze1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiAxNC41JTtcclxuICAgIHRvcDogMjhweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiA2NC41JTtcclxuICAgIHRvcDogMzYzcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI0LjUlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICByaWdodDogMzguNSU7XHJcbiAgICB0b3A6IDM2M3B4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDZ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAyNC41JTtcclxuICAgIGhlaWdodDogMjAlO1xyXG4gICAgcmlnaHQ6IDExLjUlO1xyXG4gICAgdG9wOiAzNjNweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA3e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiA2Ny41JTtcclxuICAgIHRvcDogNjY2cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wOHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI0LjUlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICByaWdodDogNDAuNSU7XHJcbiAgICB0b3A6IDY2NnB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDl7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAyNC41JTtcclxuICAgIGhlaWdodDogMjAlO1xyXG4gICAgcmlnaHQ6IDE0LjUlO1xyXG4gICAgdG9wOiA2NjZweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAxMHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI0LjUlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICByaWdodDogNjQuNSU7XHJcbiAgICB0b3A6IDEwMzVweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAxMXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI0LjUlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICByaWdodDogMzguNSU7XHJcbiAgICB0b3A6IDEwMzVweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAxMntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI0LjUlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICByaWdodDogMTEuNSU7XHJcbiAgICB0b3A6IDEwMzVweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAxM3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI0LjUlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICByaWdodDogNjcuNSU7XHJcbiAgICB0b3A6IDEzNDJweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAxNHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50IDtcclxuICAgIHdpZHRoOiAyNC41JTtcclxuICAgIGhlaWdodDogMjAlO1xyXG4gICAgcmlnaHQ6IDQwLjUlIDtcclxuICAgIHRvcDogMTM0MnB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDE1e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiAxNC41JTtcclxuICAgIHRvcDogMTM0MnB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDE2e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiA2NC41JTtcclxuICAgIHRvcDogMTY1NXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDE3e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiAzOC41JTtcclxuICAgIHRvcDogMTY1NXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDE4e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OjExLjUlO1xyXG4gICAgdG9wOiAxNjU1cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMTl7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAyNC41JTtcclxuICAgIGhlaWdodDogMjAlO1xyXG4gICAgcmlnaHQ6ICA2Ny41JTtcclxuICAgIHRvcDogMjAyNXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDIwe1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiA0MC41JTtcclxuICAgIHRvcDogMjAyNXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDIxe1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiAxNC41JTtcclxuICAgIHRvcDogMjAyNXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDIye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiA2My41JTtcclxuICAgIHRvcDogMjMzMHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDIze1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiAzNy41JTtcclxuICAgIHRvcDogMjMzMHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDI0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OjExLjUlO1xyXG4gICAgdG9wOiAyMzMwcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMjV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAyNC41JTtcclxuICAgIGhlaWdodDogMjAlO1xyXG4gICAgcmlnaHQ6IDY3LjUlO1xyXG4gICAgdG9wOiAyNjM1cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMjZ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAyNC41JTtcclxuICAgIGhlaWdodDogMjAlO1xyXG4gICAgcmlnaHQ6IDQxLjUlO1xyXG4gICAgdG9wOiAyNjM1cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMjd7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAyNC41JTtcclxuICAgIGhlaWdodDogMjAlO1xyXG4gICAgcmlnaHQ6IDE0LjUlO1xyXG4gICAgdG9wOiAyNjM1cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMjh7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAyNC41JTtcclxuICAgIGhlaWdodDogMjAlO1xyXG4gICAgcmlnaHQ6NjQuNSU7XHJcbiAgICB0b3A6IDI5OTJweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAyOXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI0LjUlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICByaWdodDogMzcuNSU7XHJcbiAgICB0b3A6IDI5OTJweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAzMHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI0LjUlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICByaWdodDogMTEuNSU7XHJcbiAgICB0b3A6IDI5OTJweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAzMXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI0LjUlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICByaWdodDogNjcuNSU7XHJcbiAgICB0b3A6IDMyOTlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAzMntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI0LjUlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICByaWdodDogNDAuNSU7XHJcbiAgICB0b3A6IDMyOTlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAzM3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI0LjUlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICByaWdodDogMTQuNSU7XHJcbiAgICB0b3A6IDMyOTlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAzNHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI0LjUlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbiAgICByaWdodDo2NC41JTtcclxuICAgIHRvcDogMzYwNHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDM1e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiAzNy41JTtcclxuICAgIHRvcDogMzYwNHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDM2e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiAxMS41JTtcclxuICAgIHRvcDogMzYwNHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDM3e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiA2Ny41JTtcclxuICAgIHRvcDogMzk3OHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDM4e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiA0MC41JTtcclxuICAgIHRvcDogMzk3OHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDM5e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiAxNC41JTtcclxuICAgIHRvcDogMzk3OHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDQwe1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiA2NC41JTtcclxuICAgIHRvcDogNDI4M3B4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDQxe1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiAzNy41JTtcclxuICAgIHRvcDogNDI4M3B4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDQye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiAxMS41JTtcclxuICAgIHRvcDogNDI4M3B4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDQze1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiA2Ny41JTtcclxuICAgIHRvcDogNDU4N3B4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDQ0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiA0MC41JTtcclxuICAgIHRvcDogNDU4N3B4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDQ1e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjQuNSU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHJpZ2h0OiAxNC41JTtcclxuICAgIHRvcDogNDU4N3B4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DesktopImgComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-desktop-img',
                templateUrl: './desktop-img.component.html',
                styleUrls: ['./desktop-img.component.css']
            }]
    }], function () { return [{ type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] }, { type: _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__["LoadingBarService"] }]; }, { init: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ["init"]
        }] }); })();


/***/ }),

/***/ "./src/app/components/exclusivo/exclusivo.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/exclusivo/exclusivo.component.ts ***!
  \*************************************************************/
/*! exports provided: ExclusivoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExclusivoComponent", function() { return ExclusivoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _modal_modal_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../modal/modal.component */ "./src/app/components/modal/modal.component.ts");
/* harmony import */ var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-loading-bar/core */ "./node_modules/@ngx-loading-bar/core/__ivy_ngcc__/fesm2015/ngx-loading-bar-core.js");
/* harmony import */ var _services_idioma_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/idioma.service */ "./src/app/services/idioma.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");







const _c0 = function () { return ["../"]; };
class ExclusivoComponent {
    constructor(loadingBarService, idiomas, dialog) {
        this.loadingBarService = loadingBarService;
        this.idiomas = idiomas;
        this.dialog = dialog;
        this.background = `assets/exclusivo/list.jpg`;
        const muestraWidth = () => {
            setTimeout(() => {
                const x = document.getElementById("background-img").getBoundingClientRect().width;
                console.log(x);
                const elements = ['p1', 'p2', 'p3'];
                const elements2 = ['p4', 'p5', 'p6'];
                const elements3 = ['p7', 'p8', 'p9'];
                for (let i in elements) {
                    const element = document.getElementById(elements[i]);
                    element.style.top = `${33 * x / 1014}px`;
                    element.style.height = `${146 * x / 1014}px`;
                }
                for (let i in elements2) {
                    const element = document.getElementById(elements2[i]);
                    element.style.top = `${189 * x / 1014}px`;
                    element.style.height = `${146 * x / 1014}px`;
                }
                for (let i in elements3) {
                    const element = document.getElementById(elements3[i]);
                    element.style.top = `${345 * x / 1014}px`;
                    element.style.height = `${146 * x / 1014}px`;
                }
                // for(let i in elements){
                //   const element = document.getElementById(elements[i])
                //   element.style.top = `${320*x/1108}px`
                // }
                // const element = document.getElementById('desktopImg')
                // element.style.top = `${1000*x/1662}px`
                // const element2 = document.getElementById('phoneImg')
                // element2.style.top = `${1959*x/1662}px`
                // const element3 = document.getElementById('smallphoneImg')
                // element3.style.top = `${2612*x/1662}px`
                // const element4 = document.getElementById('banderas')
                // element4.style.height = `${644*x/1662}px`
                muestraWidth();
            }, 400);
        };
        muestraWidth();
    }
    ngOnInit() {
    }
    openDialog(link) {
        console.log(link);
        const dialogRef = this.dialog.open(_modal_modal_component__WEBPACK_IMPORTED_MODULE_1__["ModalComponent"], {
            width: '45%',
            data: link,
            position: { top: '10px' }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }
    doSomething() {
        this.loadingBarService.complete();
    }
}
ExclusivoComponent.ɵfac = function ExclusivoComponent_Factory(t) { return new (t || ExclusivoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_2__["LoadingBarService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_idioma_service__WEBPACK_IMPORTED_MODULE_3__["IdiomasLanding"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"])); };
ExclusivoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ExclusivoComponent, selectors: [["app-exclusivo"]], decls: 11, vars: 3, consts: [["id", "background-img", 1, "main-home", 3, "src", "load"], ["id", "p1", 1, "p1", 3, "click"], ["id", "p2", 1, "p2", 3, "click"], ["id", "p3", 1, "p3", 3, "click"], ["id", "p4", 1, "p4", 3, "click"], ["id", "p5", 1, "p5", 3, "click"], ["id", "p6", 1, "p6", 3, "click"], ["id", "p7", 1, "p7", 3, "click"], ["id", "p8", 1, "p8", 3, "click"], ["id", "p9", 1, "p9", 3, "click"], ["routerLinkActive", "router-link-active", 1, "salida", 3, "routerLink"]], template: function ExclusivoComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("load", function ExclusivoComponent_Template_img_load_0_listener() { return ctx.doSomething(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExclusivoComponent_Template_button_click_1_listener() { return ctx.openDialog("assets/exclusivo/1PC.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExclusivoComponent_Template_button_click_2_listener() { return ctx.openDialog("assets/exclusivo/2PC.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExclusivoComponent_Template_button_click_3_listener() { return ctx.openDialog("assets/exclusivo/3PC.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExclusivoComponent_Template_button_click_4_listener() { return ctx.openDialog("assets/exclusivo/4PC.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExclusivoComponent_Template_button_click_5_listener() { return ctx.openDialog("assets/exclusivo/5PC.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExclusivoComponent_Template_button_click_6_listener() { return ctx.openDialog("assets/exclusivo/6PC.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExclusivoComponent_Template_button_click_7_listener() { return ctx.openDialog("assets/exclusivo/7PC.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExclusivoComponent_Template_button_click_8_listener() { return ctx.openDialog("assets/exclusivo/8PC.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExclusivoComponent_Template_button_click_9_listener() { return ctx.openDialog("assets/exclusivo/9PC.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "button", 10);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.background, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c0));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLinkActive"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLink"]], styles: [".main-home[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n   \r\n    background-position: center; \r\n    background-repeat: no-repeat; \r\n    background-size: cover; \r\n}\r\n.p1[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 25.5%;\r\n    height: 161px;\r\n    right: 64.5%;\r\n    top: 38px;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p2[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 25.5%;\r\n    height: 161px;\r\n    right: 37.5%;\r\n    top: 38px;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p3[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 25.5%;\r\n    height: 161px;\r\n    right: 10.5%;\r\n    top: 38px;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p4[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 25.5%;\r\n    height: 161px;\r\n    right: 68.5%;\r\n    top: 214px;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p5[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 25.5%;\r\n    height: 161px;\r\n    right: 40.5%;\r\n    top: 214px;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p6[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 25.5%;\r\n    height: 161px;\r\n    right: 14.5%;\r\n    top: 214px;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p7[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 25.5%;\r\n    height: 161px;\r\n    right: 64.5%;\r\n    top: 387px;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p8[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 25.5%;\r\n    height: 161px;\r\n    right: 37.5%;\r\n    top: 387px;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p9[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 25.5%;\r\n    height: 161px;\r\n    right: 10.5%;\r\n    top: 387px;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.salida[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 2.5%;\r\n    height: 34px;\r\n    right: 2.5%;\r\n    top: 28px;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9leGNsdXNpdm8vZXhjbHVzaXZvLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxXQUFXO0dBQ1osa0JBQWtCO0lBQ2pCLDJCQUEyQixFQUFFLHFCQUFxQjtJQUNsRCw0QkFBNEIsRUFBRSw0QkFBNEI7SUFDMUQsc0JBQXNCLEVBQUUsOERBQThEO0FBQzFGO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFNBQVM7SUFDVCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osU0FBUztJQUNULG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixTQUFTO0lBQ1QsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFVBQVU7SUFDVixtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osVUFBVTtJQUNWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVO0lBQ1YsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFVBQVU7SUFDVixtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osVUFBVTtJQUNWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVO0lBQ1YsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFdBQVc7SUFDWCxZQUFZO0lBQ1osV0FBVztJQUNYLFNBQVM7SUFDVCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7O0FBRWhCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9leGNsdXNpdm8vZXhjbHVzaXZvLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbi1ob21le1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgIC8qIGhlaWdodDogNjIycHg7Ki9cclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjsgLyogQ2VudGVyIHRoZSBpbWFnZSAqL1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDsgLyogRG8gbm90IHJlcGVhdCB0aGUgaW1hZ2UgKi9cclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7IC8qIFJlc2l6ZSB0aGUgYmFja2dyb3VuZCBpbWFnZSB0byBjb3ZlciB0aGUgZW50aXJlIGNvbnRhaW5lciAqL1xyXG59XHJcbi5wMXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI1LjUlO1xyXG4gICAgaGVpZ2h0OiAxNjFweDtcclxuICAgIHJpZ2h0OiA2NC41JTtcclxuICAgIHRvcDogMzhweDtcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI1LjUlO1xyXG4gICAgaGVpZ2h0OiAxNjFweDtcclxuICAgIHJpZ2h0OiAzNy41JTtcclxuICAgIHRvcDogMzhweDtcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wM3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI1LjUlO1xyXG4gICAgaGVpZ2h0OiAxNjFweDtcclxuICAgIHJpZ2h0OiAxMC41JTtcclxuICAgIHRvcDogMzhweDtcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI1LjUlO1xyXG4gICAgaGVpZ2h0OiAxNjFweDtcclxuICAgIHJpZ2h0OiA2OC41JTtcclxuICAgIHRvcDogMjE0cHg7XHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAyNS41JTtcclxuICAgIGhlaWdodDogMTYxcHg7XHJcbiAgICByaWdodDogNDAuNSU7XHJcbiAgICB0b3A6IDIxNHB4O1xyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA2e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjUuNSU7XHJcbiAgICBoZWlnaHQ6IDE2MXB4O1xyXG4gICAgcmlnaHQ6IDE0LjUlO1xyXG4gICAgdG9wOiAyMTRweDtcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wN3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDI1LjUlO1xyXG4gICAgaGVpZ2h0OiAxNjFweDtcclxuICAgIHJpZ2h0OiA2NC41JTtcclxuICAgIHRvcDogMzg3cHg7XHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDh7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAyNS41JTtcclxuICAgIGhlaWdodDogMTYxcHg7XHJcbiAgICByaWdodDogMzcuNSU7XHJcbiAgICB0b3A6IDM4N3B4O1xyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA5e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjUuNSU7XHJcbiAgICBoZWlnaHQ6IDE2MXB4O1xyXG4gICAgcmlnaHQ6IDEwLjUlO1xyXG4gICAgdG9wOiAzODdweDtcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcblxyXG4uc2FsaWRhe1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMi41JTtcclxuICAgIGhlaWdodDogMzRweDtcclxuICAgIHJpZ2h0OiAyLjUlO1xyXG4gICAgdG9wOiAyOHB4O1xyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcblxyXG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ExclusivoComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-exclusivo',
                templateUrl: './exclusivo.component.html',
                styleUrls: ['./exclusivo.component.css']
            }]
    }], function () { return [{ type: _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_2__["LoadingBarService"] }, { type: _services_idioma_service__WEBPACK_IMPORTED_MODULE_3__["IdiomasLanding"] }, { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] }]; }, null); })();


/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _modal_descarga_descarga_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../modal/descarga/descarga.component */ "./src/app/components/modal/descarga/descarga.component.ts");
/* harmony import */ var _services_idioma_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/idioma.service */ "./src/app/services/idioma.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-loading-bar/core */ "./node_modules/@ngx-loading-bar/core/__ivy_ngcc__/fesm2015/ngx-loading-bar-core.js");







class HomeComponent {
    constructor(idiomas, router, dialog, loadingBarService) {
        this.idiomas = idiomas;
        this.router = router;
        this.dialog = dialog;
        this.loadingBarService = loadingBarService;
        this.idiomasMostrar = false;
        this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
        this.loadingBarService.start();
    }
    ngOnInit() {
        const muestraWidth = () => {
            setTimeout(() => {
                const x = document.getElementById("background-img").getBoundingClientRect().width;
                console.log(x);
                const elements = ['veridiomas', 'donacion', 'contacto'];
                for (let i in elements) {
                    const element = document.getElementById(elements[i]);
                    element.style.top = `${320 * x / 1108}px`;
                }
                const element = document.getElementById('desktopImg');
                element.style.top = `${1000 * x / 1662}px`;
                const element2 = document.getElementById('phoneImg');
                element2.style.top = `${1959 * x / 1662}px`;
                const element3 = document.getElementById('smallphoneImg');
                element3.style.top = `${2612 * x / 1662}px`;
                const element4 = document.getElementById('banderas');
                element4.style.height = `${351 * x / 1022}px`;
                element4.style.top = `${48 * x / 1022}px`;
                const element5 = document.getElementById('bandera');
                element5.style.width = `${90 * x / 1662}px`;
                element5.style.marginLeft = `${33 * x / 1662}px`;
                const banderas = ['bandera1', 'bandera2', 'bandera3', 'bandera4', 'bandera5'];
                for (let i in banderas) {
                    const e = document.getElementById(banderas[i]);
                    e.style.width = `${90 * x / 1662}px`;
                    e.style.marginLeft = `${33 * x / 1662}px`;
                }
                muestraWidth();
            }, 400);
        };
        muestraWidth();
    }
    veridiomas() { this.idiomasMostrar = !this.idiomasMostrar; }
    donacion() {
        this.router.navigate(['/cabina-pago']);
        this.loadingBarService.start();
    }
    contacto() {
        this.router.navigate(['/contacto']);
        this.loadingBarService.start();
    }
    setingles() {
        // this.idiomas.idioma==="english"?null:this.loadingBarService.start();
        const dialogRef = this.dialog.open(_modal_descarga_descarga_component__WEBPACK_IMPORTED_MODULE_1__["DescargaComponent"], {
            width: '40%'
        });
        this.idiomas.idioma = "english";
        this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
    }
    setespanol() {
        // this.idiomas.idioma==="espanol"?null:this.loadingBarService.start();
        const dialogRef = this.dialog.open(_modal_descarga_descarga_component__WEBPACK_IMPORTED_MODULE_1__["DescargaComponent"], {
            width: '40%'
        });
        this.idiomas.idioma = "espanol";
        this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
    }
    setaleman() {
        // this.idiomas.idioma==="aleman"?null:this.loadingBarService.start();
        const dialogRef = this.dialog.open(_modal_descarga_descarga_component__WEBPACK_IMPORTED_MODULE_1__["DescargaComponent"], {
            width: '40%'
        });
        this.idiomas.idioma = "aleman";
        this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
    }
    setitaliano() {
        // this.idiomas.idioma==="italiano"?null:this.loadingBarService.start();
        const dialogRef = this.dialog.open(_modal_descarga_descarga_component__WEBPACK_IMPORTED_MODULE_1__["DescargaComponent"], {
            width: '40%'
        });
        this.idiomas.idioma = "italiano";
        this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
    }
    setportugues() {
        // this.idiomas.idioma==="portugues"?null:this.loadingBarService.start();
        const dialogRef = this.dialog.open(_modal_descarga_descarga_component__WEBPACK_IMPORTED_MODULE_1__["DescargaComponent"], {
            width: '40%'
        });
        this.idiomas.idioma = "portugues";
        this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
    }
    setfrances() {
        /* this.idiomas.idioma==="frances"?null:this.loadingBarService.start(); */
        const dialogRef = this.dialog.open(_modal_descarga_descarga_component__WEBPACK_IMPORTED_MODULE_1__["DescargaComponent"], {
            width: '40%'
        });
        this.idiomas.idioma = "frances";
        this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
    }
    desktopImg() {
        this.router.navigate(['/wallpers-escritorio']);
        this.loadingBarService.start();
    }
    phoneImg() {
        this.router.navigate(['/wallpers-telefono']);
        this.loadingBarService.start();
    }
    smallphoneImg() {
        this.router.navigate(['/wallpers-telefono-pequeno']);
        this.loadingBarService.start();
    }
    doSomething() {
        // const dialogRef = this.dialog.open(DescargaComponent, {
        //   width: '40%'
        // });
        this.loadingBarService.complete();
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_idioma_service__WEBPACK_IMPORTED_MODULE_2__["IdiomasLanding"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_5__["LoadingBarService"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 26, vars: 3, consts: [["id", "background-img", 1, "main-home", 3, "src", "load"], ["id", "veridiomas", 1, "veridiomas", 3, "click"], ["id", "donacion", 1, "donacion", 3, "click"], ["id", "contacto", 1, "contacto", 3, "click"], ["id", "desktopImg", 1, "desktopImg", 3, "click"], ["id", "phoneImg", 1, "phoneImg", 3, "click"], ["id", "smallphoneImg", 1, "smallphoneImg", 3, "click"], ["id", "banderas", 1, "banderas-hide", "row", "text-center", 2, "justify-content", "center"], [1, "col-12", "text-center", 2, "margin-top", "25%!important"], ["id", "bandera5", 1, "boton-bandera", 3, "click"], ["src", "./assets/banderas/ingles.png", 1, "bandera"], [1, "col-12", 2, "margin-top", "25%!important"], ["id", "bandera4", 1, "boton-bandera", 3, "click"], ["src", "./assets/banderas/espanol.png", 1, "bandera"], ["id", "bandera", 1, "boton-bandera", 3, "click"], ["src", "./assets/banderas/aleman.png", 1, "bandera"], ["id", "bandera1", 1, "boton-bandera", 3, "click"], ["src", "./assets/banderas/italiano.png", 1, "bandera"], ["id", "bandera2", 1, "boton-bandera", 3, "click"], ["src", "./assets/banderas/portugues.png", 1, "bandera"], ["id", "bandera3", 1, "boton-bandera", 3, "click"], ["src", "./assets/banderas/frances.png", 1, "bandera"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("load", function HomeComponent_Template_img_load_0_listener() { return ctx.doSomething(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_1_listener() { return ctx.veridiomas(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_2_listener() { return ctx.donacion(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_3_listener() { return ctx.contacto(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_4_listener() { return ctx.desktopImg(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_5_listener() { return ctx.phoneImg(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_6_listener() { return ctx.smallphoneImg(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_9_listener() { return ctx.setingles(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "img", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_12_listener() { return ctx.setespanol(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "img", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_15_listener() { return ctx.setaleman(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "button", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_18_listener() { return ctx.setitaliano(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "img", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_21_listener() { return ctx.setportugues(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_24_listener() { return ctx.setfrances(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.background, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("banderas", ctx.idiomasMostrar);
    } }, styles: [".main-home[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    \r\n    background-position: center; \r\n    background-repeat: no-repeat; \r\n    background-size: cover; \r\n}\r\n.veridiomas[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10%;\r\n    height: 10%;\r\n    right: 83%;\r\n    \r\n    \r\n    border: transparent;\r\n    outline:none;\r\n    z-index: 200;\r\n}\r\n.donacion[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10%;\r\n    height: 10%;\r\n    right: 71%;\r\n    top: 670px;\r\n    \r\n    border: transparent;\r\n    outline:none;\r\n    z-index: 200;\r\n}\r\n.contacto[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10%;\r\n    height: 10%;\r\n    right: 59%;\r\n    top: 670px;\r\n    \r\n    border: transparent;\r\n    outline:none;\r\n    z-index: 200;\r\n}\r\n.banderas-hide[_ngcontent-%COMP%]{\r\n    height: 866px;\r\n    width: 0%;\r\n    right: 0;\r\n    top:0;\r\n    position: absolute;\r\n    background-color:rgba(0,0,0,0.4);\r\n    transition: 1s ease-in-out;\r\n}\r\n.banderas[_ngcontent-%COMP%]{\r\n    height: 351px;\r\n    width: 10% !important;\r\n    right: 0;\r\n    top:48px;\r\n    position: absolute;\r\n    background-color:rgba(0,0,0,0.4);\r\n    transition: 1s ease-in-out;\r\n}\r\n.bandera-ingles[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    margin-top:27%!important;\r\n}\r\n.bandera-espanol[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    margin-top:27%!important;\r\n}\r\n.bandera-aleman[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    margin-top:27%!important;\r\n}\r\n.bandera-italiano[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    margin-top:27%!important;\r\n}\r\n.bandera-portugues[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    margin-top:27%!important;\r\n}\r\n.bandera-frances[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    margin-top:27%!important;\r\n}\r\n.boton-bandera[_ngcontent-%COMP%]{\r\n    background: transparent;\r\n    outline: none;\r\n    border: transparent;\r\n    \r\n}\r\n.desktopImg[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 12%;\r\n    height: 10%;\r\n    right: 33%;\r\n    top: 1388px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.phoneImg[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 12%;\r\n    height: 10%;\r\n    right: 76%;\r\n    top: 2688px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.smallphoneImg[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 12%;\r\n    height: 10%;\r\n    right: 15%;\r\n    top: 3577px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.bandera[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ob21lL2hvbWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFdBQVc7SUFDWCxvQkFBb0I7SUFDcEIsMkJBQTJCLEVBQUUscUJBQXFCO0lBQ2xELDRCQUE0QixFQUFFLDRCQUE0QjtJQUMxRCxzQkFBc0IsRUFBRSw4REFBOEQ7QUFDMUY7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsVUFBVTtJQUNWLFdBQVc7SUFDWCxVQUFVO0lBQ1YsZ0JBQWdCOztJQUVoQixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsVUFBVTtJQUNWLFdBQVc7SUFDWCxVQUFVO0lBQ1YsVUFBVTs7SUFFVixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsVUFBVTtJQUNWLFdBQVc7SUFDWCxVQUFVO0lBQ1YsVUFBVTs7SUFFVixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGFBQWE7SUFDYixTQUFTO0lBQ1QsUUFBUTtJQUNSLEtBQUs7SUFDTCxrQkFBa0I7SUFDbEIsZ0NBQWdDO0lBSWhDLDBCQUEwQjtBQUM5QjtBQUNBO0lBQ0ksYUFBYTtJQUNiLHFCQUFxQjtJQUNyQixRQUFRO0lBQ1IsUUFBUTtJQUNSLGtCQUFrQjtJQUNsQixnQ0FBZ0M7SUFJaEMsMEJBQTBCO0FBQzlCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSx1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLG1CQUFtQjs7QUFFdkI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsVUFBVTtJQUNWLFdBQVc7SUFDWCxVQUFVO0lBQ1YsV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsVUFBVTtJQUNWLFdBQVc7SUFDWCxVQUFVO0lBQ1YsV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsVUFBVTtJQUNWLFdBQVc7SUFDWCxVQUFVO0lBQ1YsV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7QUFFQTtJQUNJLFdBQVc7QUFDZiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbi1ob21le1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICAvKiBoZWlnaHQ6IDUwMDBweDsgKi9cclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjsgLyogQ2VudGVyIHRoZSBpbWFnZSAqL1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDsgLyogRG8gbm90IHJlcGVhdCB0aGUgaW1hZ2UgKi9cclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7IC8qIFJlc2l6ZSB0aGUgYmFja2dyb3VuZCBpbWFnZSB0byBjb3ZlciB0aGUgZW50aXJlIGNvbnRhaW5lciAqL1xyXG59XHJcbi52ZXJpZGlvbWFze1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAlO1xyXG4gICAgaGVpZ2h0OiAxMCU7XHJcbiAgICByaWdodDogODMlO1xyXG4gICAgLyogdG9wOiA2NzBweDsgKi9cclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6bm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4uZG9uYWNpb257XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMCU7XHJcbiAgICBoZWlnaHQ6IDEwJTtcclxuICAgIHJpZ2h0OiA3MSU7XHJcbiAgICB0b3A6IDY3MHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTpub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5jb250YWN0b3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwJTtcclxuICAgIGhlaWdodDogMTAlO1xyXG4gICAgcmlnaHQ6IDU5JTtcclxuICAgIHRvcDogNjcwcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOm5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLmJhbmRlcmFzLWhpZGV7XHJcbiAgICBoZWlnaHQ6IDg2NnB4O1xyXG4gICAgd2lkdGg6IDAlO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICB0b3A6MDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6cmdiYSgwLDAsMCwwLjQpO1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAxcyBlYXNlLWluLW91dDtcclxuICAgIC1tb3otdHJhbnNpdGlvbjogMXMgZWFzZS1pbi1vdXQ7XHJcbiAgICAtby10cmFuc2l0aW9uOiAxcyBlYXNlLWluLW91dDtcclxuICAgIHRyYW5zaXRpb246IDFzIGVhc2UtaW4tb3V0O1xyXG59XHJcbi5iYW5kZXJhc3tcclxuICAgIGhlaWdodDogMzUxcHg7XHJcbiAgICB3aWR0aDogMTAlICFpbXBvcnRhbnQ7XHJcbiAgICByaWdodDogMDtcclxuICAgIHRvcDo0OHB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjpyZ2JhKDAsMCwwLDAuNCk7XHJcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IDFzIGVhc2UtaW4tb3V0O1xyXG4gICAgLW1vei10cmFuc2l0aW9uOiAxcyBlYXNlLWluLW91dDtcclxuICAgIC1vLXRyYW5zaXRpb246IDFzIGVhc2UtaW4tb3V0O1xyXG4gICAgdHJhbnNpdGlvbjogMXMgZWFzZS1pbi1vdXQ7XHJcbn1cclxuLmJhbmRlcmEtaW5nbGVze1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tdG9wOjI3JSFpbXBvcnRhbnQ7XHJcbn1cclxuLmJhbmRlcmEtZXNwYW5vbHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDoyNyUhaW1wb3J0YW50O1xyXG59XHJcbi5iYW5kZXJhLWFsZW1hbntcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDoyNyUhaW1wb3J0YW50O1xyXG59XHJcbi5iYW5kZXJhLWl0YWxpYW5ve1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tdG9wOjI3JSFpbXBvcnRhbnQ7XHJcbn1cclxuLmJhbmRlcmEtcG9ydHVndWVze1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tdG9wOjI3JSFpbXBvcnRhbnQ7XHJcbn1cclxuLmJhbmRlcmEtZnJhbmNlc3tcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDoyNyUhaW1wb3J0YW50O1xyXG59XHJcbi5ib3Rvbi1iYW5kZXJhe1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIFxyXG59XHJcbi5kZXNrdG9wSW1ne1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTIlO1xyXG4gICAgaGVpZ2h0OiAxMCU7XHJcbiAgICByaWdodDogMzMlO1xyXG4gICAgdG9wOiAxMzg4cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5waG9uZUltZ3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEyJTtcclxuICAgIGhlaWdodDogMTAlO1xyXG4gICAgcmlnaHQ6IDc2JTtcclxuICAgIHRvcDogMjY4OHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4uc21hbGxwaG9uZUltZ3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEyJTtcclxuICAgIGhlaWdodDogMTAlO1xyXG4gICAgcmlnaHQ6IDE1JTtcclxuICAgIHRvcDogMzU3N3B4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG5cclxuLmJhbmRlcmF7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
                styleUrls: ['./home.component.css']
            }]
    }], function () { return [{ type: _services_idioma_service__WEBPACK_IMPORTED_MODULE_2__["IdiomasLanding"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }, { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] }, { type: _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_5__["LoadingBarService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/components/modal/descarga/descarga.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/modal/descarga/descarga.component.ts ***!
  \*****************************************************************/
/*! exports provided: DescargaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DescargaComponent", function() { return DescargaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");




function DescargaComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx_r0.background, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
function DescargaComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx_r1.background2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
class DescargaComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
        this.background = 'assets/figurin/1.png';
        this.background2 = 'assets/figurin/2.png';
        this.boolean = true;
        this.repeticiones = 5;
        this.cambiarimg();
        dialogRef.disableClose = true;
    }
    cambiarimg() {
        setTimeout(() => { this.boolean = !this.boolean; this.cambiarimg(); }, 1000);
        this.repeticiones === 0 ? this.dialogRef.close() : this.repeticiones -= 1;
    }
}
DescargaComponent.ɵfac = function DescargaComponent_Factory(t) { return new (t || DescargaComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"])); };
DescargaComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: DescargaComponent, selectors: [["app-descarga"]], decls: 2, vars: 2, consts: [[4, "ngIf"], [1, "main-home", 3, "src"]], template: function DescargaComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, DescargaComponent_div_0_Template, 2, 1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, DescargaComponent_div_1_Template, 2, 1, "div", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.boolean);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.boolean);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"]], styles: [".main-home[_ngcontent-%COMP%]{\r\n    width: 250px;\r\n    background-color: transparent;\r\n    background-position: center; \r\n    background-repeat: no-repeat; \r\n    background-size: cover; \r\n    right: 40%;\r\n    top: 40%;\r\n    position: absolute;\r\n\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbC9kZXNjYXJnYS9kZXNjYXJnYS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtJQUNaLDZCQUE2QjtJQUM3QiwyQkFBMkIsRUFBRSxxQkFBcUI7SUFDbEQsNEJBQTRCLEVBQUUsNEJBQTRCO0lBQzFELHNCQUFzQixFQUFFLDhEQUE4RDtJQUN0RixVQUFVO0lBQ1YsUUFBUTtJQUNSLGtCQUFrQjs7QUFFdEIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vZGFsL2Rlc2NhcmdhL2Rlc2NhcmdhLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbi1ob21le1xyXG4gICAgd2lkdGg6IDI1MHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7IC8qIENlbnRlciB0aGUgaW1hZ2UgKi9cclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7IC8qIERvIG5vdCByZXBlYXQgdGhlIGltYWdlICovXHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyOyAvKiBSZXNpemUgdGhlIGJhY2tncm91bmQgaW1hZ2UgdG8gY292ZXIgdGhlIGVudGlyZSBjb250YWluZXIgKi9cclxuICAgIHJpZ2h0OiA0MCU7XHJcbiAgICB0b3A6IDQwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHJcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DescargaComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-descarga',
                templateUrl: './descarga.component.html',
                styleUrls: ['./descarga.component.css']
            }]
    }], function () { return [{ type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"] }]; }, null); })();


/***/ }),

/***/ "./src/app/components/modal/modal.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/modal/modal.component.ts ***!
  \*****************************************************/
/*! exports provided: ModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return ModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _descarga_descarga_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./descarga/descarga.component */ "./src/app/components/modal/descarga/descarga.component.ts");
/* harmony import */ var _services_idioma_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/idioma.service */ "./src/app/services/idioma.service.ts");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");








const _c0 = function () { return ["../"]; };
class ModalComponent {
    constructor(dialog, dialogRef, data, idioma) {
        this.dialog = dialog;
        this.dialogRef = dialogRef;
        this.data = data;
        this.idioma = idioma;
        this.background = 'assets/popup/english.png';
        this.title = "GO TO SITE START";
        const muestraWidth = () => {
            setTimeout(() => {
                const x = document.getElementById("background-img").getBoundingClientRect().width;
                console.log(x);
                const elements = ['link', 'close', 'inicio'];
                const element = document.getElementById(elements[0]);
                element.style.top = `${373.573 * x / 1087}px`;
                element.style.height = `${29 * x / 1087}px`;
                const element2 = document.getElementById(elements[1]);
                element2.style.top = `${419.365 * x / 1087}px`;
                element2.style.height = `${29 * x / 1087}px`;
                const element3 = document.getElementById(elements[2]);
                element3.style.top = `${464.293 * x / 1087}px`;
                element3.style.height = `${29 * x / 1087}px`;
                muestraWidth();
            }, 400);
        };
        muestraWidth();
        console.log(this.data);
        this.background = `assets/popup/${this.idioma.idioma}.png`;
        switch (this.idioma.idioma) {
            case "english":
                this.title = `GO TO SITE START`;
                break;
            case "espanol":
                this.title = `IR AL INICIO DEL PORTAL`;
                break;
            case "aleman":
                this.title = `GEHE ZUM START DER SITE`;
                break;
            case "italiano":
                this.title = ` VAI ALL’INIZIO DEL SITO`;
                break;
            case "portugues":
                this.title = `IR PARA O INÌCIO DO SITE`;
                break;
            case "frances":
                this.title = `ALLER AU DÈBUT DU SITE`;
                break;
        }
    }
    download() {
        const dialogRef = this.dialog.open(_descarga_descarga_component__WEBPACK_IMPORTED_MODULE_2__["DescargaComponent"], {
            width: '40%'
        });
    }
}
ModalComponent.ɵfac = function ModalComponent_Factory(t) { return new (t || ModalComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_idioma_service__WEBPACK_IMPORTED_MODULE_3__["IdiomasLanding"])); };
ModalComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ModalComponent, selectors: [["app-modal"]], decls: 5, vars: 5, consts: [["id", "background-img", 1, "main-home", 3, "src"], ["mat-dialog-actions", ""], ["mat-dialog-close", "", "id", "link", "download", "halsey-wallpaper", 1, "link", 3, "href", "click"], ["mat-button", "", "mat-dialog-close", "", "id", "close", "cdkFocusInitial", "", 1, "close"], ["mat-button", "", "mat-dialog-close", "", "id", "inicio", "cdkFocusInitial", "", 1, "close-2", 3, "routerLink", "title"]], template: function ModalComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "img", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ModalComponent_Template_a_click_2_listener() { return ctx.download(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.background, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("href", ctx.data, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c0))("title", ctx.title);
    } }, directives: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogActions"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogClose"], _angular_material_button__WEBPACK_IMPORTED_MODULE_4__["MatButton"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLink"]], styles: [".main-home[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    \r\n    background-color: transparent;\r\n    background-position: center; \r\n    background-repeat: no-repeat; \r\n    background-size: cover; \r\n}\r\n.link[_ngcontent-%COMP%]{\r\n    background-color: transparent;\r\n    width: 14%;\r\n    cursor : url('CURSOR-GUITARRA.png'), url('CURSOR-GUITARRA.cur'),auto;\r\n    height: 35px;\r\n    top: 376px;\r\n    right: 38%;\r\n    position: absolute;\r\n}\r\n.close[_ngcontent-%COMP%]{\r\n    background-color: transparent;\r\n    width: 14%;\r\n    cursor : url('CURSOR-GUITARRA.png'), url('CURSOR-GUITARRA.cur'),auto;\r\n    height: 35px;\r\n    top: 418px;\r\n    right: 38%;\r\n    position: absolute;\r\n}\r\n.close-2[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 5%;\r\n    height: 38px;\r\n    cursor : url('CURSOR-GUITARRA.png'), url('CURSOR-GUITARRA.cur'),auto;\r\n    top: 464px;\r\n    right: 42%;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbC9tb2RhbC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksV0FBVztJQUNYLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsMkJBQTJCLEVBQUUscUJBQXFCO0lBQ2xELDRCQUE0QixFQUFFLDRCQUE0QjtJQUMxRCxzQkFBc0IsRUFBRSw4REFBOEQ7QUFDMUY7QUFDQTtJQUNJLDZCQUE2QjtJQUM3QixVQUFVO0lBQ1Ysb0VBQW9IO0lBQ3BILFlBQVk7SUFDWixVQUFVO0lBQ1YsVUFBVTtJQUNWLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksNkJBQTZCO0lBQzdCLFVBQVU7SUFDVixvRUFBb0g7SUFDcEgsWUFBWTtJQUNaLFVBQVU7SUFDVixVQUFVO0lBQ1Ysa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFNBQVM7SUFDVCxZQUFZO0lBQ1osb0VBQW9IO0lBQ3BILFVBQVU7SUFDVixVQUFVO0lBQ1YsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9tb2RhbC9tb2RhbC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW4taG9tZXtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLyogaGVpZ2h0OiA1MDBweDsgKi9cclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyOyAvKiBDZW50ZXIgdGhlIGltYWdlICovXHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0OyAvKiBEbyBub3QgcmVwZWF0IHRoZSBpbWFnZSAqL1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjsgLyogUmVzaXplIHRoZSBiYWNrZ3JvdW5kIGltYWdlIHRvIGNvdmVyIHRoZSBlbnRpcmUgY29udGFpbmVyICovXHJcbn1cclxuLmxpbmt7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxNCU7XHJcbiAgICBjdXJzb3IgOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvY3Vyc29ycy9DVVJTT1ItR1VJVEFSUkEucG5nXCIpLCB1cmwoXCIuLi8uLi8uLi9hc3NldHMvY3Vyc29ycy9DVVJTT1ItR1VJVEFSUkEuY3VyXCIpLGF1dG87XHJcbiAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICB0b3A6IDM3NnB4O1xyXG4gICAgcmlnaHQ6IDM4JTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG4uY2xvc2V7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxNCU7XHJcbiAgICBjdXJzb3IgOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvY3Vyc29ycy9DVVJTT1ItR1VJVEFSUkEucG5nXCIpLCB1cmwoXCIuLi8uLi8uLi9hc3NldHMvY3Vyc29ycy9DVVJTT1ItR1VJVEFSUkEuY3VyXCIpLGF1dG87XHJcbiAgICBoZWlnaHQ6IDM1cHg7XHJcbiAgICB0b3A6IDQxOHB4O1xyXG4gICAgcmlnaHQ6IDM4JTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG4uY2xvc2UtMntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDUlO1xyXG4gICAgaGVpZ2h0OiAzOHB4O1xyXG4gICAgY3Vyc29yIDogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL2N1cnNvcnMvQ1VSU09SLUdVSVRBUlJBLnBuZ1wiKSwgdXJsKFwiLi4vLi4vLi4vYXNzZXRzL2N1cnNvcnMvQ1VSU09SLUdVSVRBUlJBLmN1clwiKSxhdXRvO1xyXG4gICAgdG9wOiA0NjRweDtcclxuICAgIHJpZ2h0OiA0MiU7XHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ModalComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-modal',
                templateUrl: './modal.component.html',
                styleUrls: ['./modal.component.css']
            }]
    }], function () { return [{ type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialog"] }, { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"] }, { type: undefined, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
                args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"]]
            }] }, { type: _services_idioma_service__WEBPACK_IMPORTED_MODULE_3__["IdiomasLanding"] }]; }, null); })();


/***/ }),

/***/ "./src/app/components/phone-img/phone-img.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/phone-img/phone-img.component.ts ***!
  \*************************************************************/
/*! exports provided: PhoneImgComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhoneImgComponent", function() { return PhoneImgComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _modal_modal_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../modal/modal.component */ "./src/app/components/modal/modal.component.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-loading-bar/core */ "./node_modules/@ngx-loading-bar/core/__ivy_ngcc__/fesm2015/ngx-loading-bar-core.js");





const _c0 = ["init"];
class PhoneImgComponent {
    constructor(dialog, loadingBarService) {
        this.dialog = dialog;
        this.loadingBarService = loadingBarService;
        const muestraWidth = () => {
            setTimeout(() => {
                const x = document.getElementById("background-img").getBoundingClientRect().width;
                console.log(x);
                const elements = ['p1', 'p2', 'p3', 'p4', 'p5', 'p6'];
                const elements2 = ['p7', 'p8', 'p9', 'p10', 'p11', 'p12'];
                const elements3 = ['p13', 'p14', 'p15', 'p16', 'p17', 'p18'];
                const elements4 = ['p19', 'p20', 'p21', 'p22', 'p23', 'p24'];
                const elements5 = ['p25', 'p26', 'p27', 'p28', 'p29', 'p30'];
                const elements6 = ['p31', 'p32', 'p33', 'p34', 'p35', 'p36'];
                const elements7 = ['p37', 'p38', 'p39', 'p40', 'p41', 'p42'];
                const elements8 = ['p43', 'p44', 'p45', 'p46', 'p47', 'p48'];
                for (let i in elements) {
                    const element = document.getElementById(elements[i]);
                    element.style.top = `${26 * x / 1108}px`;
                    element.style.height = `${231 * x / 1108}px`;
                }
                for (let i in elements2) {
                    const element = document.getElementById(elements2[i]);
                    element.style.top = `${269 * x / 1108}px`;
                    element.style.height = `${231 * x / 1108}px`;
                }
                for (let i in elements3) {
                    const element = document.getElementById(elements3[i]);
                    element.style.top = `${542 * x / 1108}px`;
                    element.style.height = `${231 * x / 1108}px`;
                }
                for (let i in elements4) {
                    const element = document.getElementById(elements4[i]);
                    element.style.top = `${783 * x / 1108}px`;
                    element.style.height = `${231 * x / 1108}px`;
                }
                for (let i in elements5) {
                    const element = document.getElementById(elements5[i]);
                    element.style.top = `${1057 * x / 1108}px`;
                    element.style.height = `${231 * x / 1108}px`;
                }
                for (let i in elements6) {
                    const element = document.getElementById(elements6[i]);
                    element.style.top = `${1303 * x / 1108}px`;
                    element.style.height = `${231 * x / 1108}px`;
                }
                for (let i in elements7) {
                    const element = document.getElementById(elements7[i]);
                    element.style.top = `${1584 * x / 1108}px`;
                    element.style.height = `${231 * x / 1108}px`;
                }
                for (let i in elements8) {
                    const element = document.getElementById(elements8[i]);
                    element.style.top = `${1833 * x / 1108}px`;
                    element.style.height = `${231 * x / 1108}px`;
                }
                // const elements= ['veridiomas','donacion','contacto']
                // for(let i in elements){
                //   const element = document.getElementById(elements[i])
                //   element.style.top = `${320*x/1108}px`
                // }
                muestraWidth();
            }, 400);
        };
        muestraWidth();
        setTimeout(() => {
            this.init.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
        }, 500);
    }
    openDialog(link) {
        console.log(link);
        const dialogRef = this.dialog.open(_modal_modal_component__WEBPACK_IMPORTED_MODULE_1__["ModalComponent"], {
            width: '45%',
            data: link,
            position: { top: '10px' }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }
    doSomething() {
        // const dialogRef = this.dialog.open(DescargaComponent, {
        //   width: '40%'
        // });
        this.loadingBarService.complete();
    }
}
PhoneImgComponent.ɵfac = function PhoneImgComponent_Factory(t) { return new (t || PhoneImgComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__["LoadingBarService"])); };
PhoneImgComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PhoneImgComponent, selectors: [["app-phone-img"]], viewQuery: function PhoneImgComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
    } if (rf & 2) {
        var _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.init = _t.first);
    } }, decls: 51, vars: 0, consts: [[2, "top", "0"], ["init", ""], ["src", "assets/phoneImg/list.jpg", "id", "background-img", 1, "main-home", 3, "load"], ["id", "p1", 1, "p1", 3, "click"], ["id", "p2", 1, "p2", 3, "click"], ["id", "p3", 1, "p3", 3, "click"], ["id", "p4", 1, "p4", 3, "click"], ["id", "p5", 1, "p5", 3, "click"], ["id", "p6", 1, "p6", 3, "click"], ["id", "p7", 1, "p7", 3, "click"], ["id", "p8", 1, "p8", 3, "click"], ["id", "p9", 1, "p9", 3, "click"], ["id", "p10", 1, "p10", 3, "click"], ["id", "p11", 1, "p11", 3, "click"], ["id", "p12", 1, "p12", 3, "click"], ["id", "p13", 1, "p13", 3, "click"], ["id", "p14", 1, "p14", 3, "click"], ["id", "p15", 1, "p15", 3, "click"], ["id", "p16", 1, "p16", 3, "click"], ["id", "p17", 1, "p17", 3, "click"], ["id", "p18", 1, "p18", 3, "click"], ["id", "p19", 1, "p19", 3, "click"], ["id", "p20", 1, "p20", 3, "click"], ["id", "p21", 1, "p21", 3, "click"], ["id", "p22", 1, "p22", 3, "click"], ["id", "p23", 1, "p23", 3, "click"], ["id", "p24", 1, "p24", 3, "click"], ["id", "p25", 1, "p25", 3, "click"], ["id", "p26", 1, "p26", 3, "click"], ["id", "p27", 1, "p27", 3, "click"], ["id", "p28", 1, "p28", 3, "click"], ["id", "p29", 1, "p29", 3, "click"], ["id", "p30", 1, "p30", 3, "click"], ["id", "p31", 1, "p31", 3, "click"], ["id", "p32", 1, "p32", 3, "click"], ["id", "p33", 1, "p33", 3, "click"], ["id", "p34", 1, "p34", 3, "click"], ["id", "p35", 1, "p35", 3, "click"], ["id", "p36", 1, "p36", 3, "click"], ["id", "p37", 1, "p37", 3, "click"], ["id", "p38", 1, "p38", 3, "click"], ["id", "p39", 1, "p39", 3, "click"], ["id", "p40", 1, "p40", 3, "click"], ["id", "p41", 1, "p41", 3, "click"], ["id", "p42", 1, "p42", 3, "click"], ["id", "p43", 1, "p43", 3, "click"], ["id", "p44", 1, "p44", 3, "click"], ["id", "p45", 1, "p45", 3, "click"], ["id", "p46", 1, "p46", 3, "click"], ["id", "p47", 1, "p47", 3, "click"], ["id", "p48", 1, "p48", 3, "click"]], template: function PhoneImgComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 0, 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("load", function PhoneImgComponent_Template_img_load_2_listener() { return ctx.doSomething(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_3_listener() { return ctx.openDialog("assets/phoneImg/1LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_4_listener() { return ctx.openDialog("assets/phoneImg/2LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_5_listener() { return ctx.openDialog("assets/phoneImg/3LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_6_listener() { return ctx.openDialog("assets/phoneImg/4LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_7_listener() { return ctx.openDialog("assets/phoneImg/5LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_8_listener() { return ctx.openDialog("assets/phoneImg/6LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_9_listener() { return ctx.openDialog("assets/phoneImg/7LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_10_listener() { return ctx.openDialog("assets/phoneImg/8LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_11_listener() { return ctx.openDialog("assets/phoneImg/9LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_12_listener() { return ctx.openDialog("assets/phoneImg/10LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_13_listener() { return ctx.openDialog("assets/phoneImg/11LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_14_listener() { return ctx.openDialog("assets/phoneImg/12LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_15_listener() { return ctx.openDialog("assets/phoneImg/13LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "button", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_16_listener() { return ctx.openDialog("assets/phoneImg/14LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_17_listener() { return ctx.openDialog("assets/phoneImg/15LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_18_listener() { return ctx.openDialog("assets/phoneImg/16LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "button", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_19_listener() { return ctx.openDialog("assets/phoneImg/17LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_20_listener() { return ctx.openDialog("assets/phoneImg/18LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_21_listener() { return ctx.openDialog("assets/phoneImg/19LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_22_listener() { return ctx.openDialog("assets/phoneImg/20LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "button", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_23_listener() { return ctx.openDialog("assets/phoneImg/21LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "button", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_24_listener() { return ctx.openDialog("assets/phoneImg/22LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "button", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_25_listener() { return ctx.openDialog("assets/phoneImg/23LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "button", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_26_listener() { return ctx.openDialog("assets/phoneImg/24LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "button", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_27_listener() { return ctx.openDialog("assets/phoneImg/25LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "button", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_28_listener() { return ctx.openDialog("assets/phoneImg/26LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "button", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_29_listener() { return ctx.openDialog("assets/phoneImg/27LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "button", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_30_listener() { return ctx.openDialog("assets/phoneImg/28LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "button", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_31_listener() { return ctx.openDialog("assets/phoneImg/29LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "button", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_32_listener() { return ctx.openDialog("assets/phoneImg/30LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "button", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_33_listener() { return ctx.openDialog("assets/phoneImg/31LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "button", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_34_listener() { return ctx.openDialog("assets/phoneImg/32LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "button", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_35_listener() { return ctx.openDialog("assets/phoneImg/33LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "button", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_36_listener() { return ctx.openDialog("assets/phoneImg/34LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "button", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_37_listener() { return ctx.openDialog("assets/phoneImg/35LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "button", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_38_listener() { return ctx.openDialog("assets/phoneImg/36LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "button", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_39_listener() { return ctx.openDialog("assets/phoneImg/37LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "button", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_40_listener() { return ctx.openDialog("assets/phoneImg/38LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "button", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_41_listener() { return ctx.openDialog("assets/phoneImg/39LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "button", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_42_listener() { return ctx.openDialog("assets/phoneImg/40LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "button", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_43_listener() { return ctx.openDialog("assets/phoneImg/41LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "button", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_44_listener() { return ctx.openDialog("assets/phoneImg/42LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "button", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_45_listener() { return ctx.openDialog("assets/phoneImg/43LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "button", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_46_listener() { return ctx.openDialog("assets/phoneImg/44LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "button", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_47_listener() { return ctx.openDialog("assets/phoneImg/45LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "button", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_48_listener() { return ctx.openDialog("assets/phoneImg/46LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "button", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_49_listener() { return ctx.openDialog("assets/phoneImg/47LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "button", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhoneImgComponent_Template_button_click_50_listener() { return ctx.openDialog("assets/phoneImg/48LS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: [".main-home[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    \r\n    background-position: center; \r\n    background-repeat: no-repeat; \r\n    background-size: cover; \r\n}\r\n\r\n.main-home[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    \r\n    background-position: center; \r\n    background-repeat: no-repeat; \r\n    background-size: cover; \r\n}\r\n\r\n.p1[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 78.5%;\r\n    top: 59px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p2[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right:66.5%;\r\n    top: 59px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p3[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 54.5%;\r\n    top: 59px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p4[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 42.5%;\r\n    top: 59px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p5[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right:30.5%;\r\n    top: 59px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p6[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 18.5%;\r\n    top: 59px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p7[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 72.5%;\r\n    top: 632px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p8[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right:60.5%;\r\n    top: 632px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p9[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 48.5%;\r\n    top: 632px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p10[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right:36.5%;\r\n    top: 632px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p11[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 24.5%;\r\n    top: 632px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p12[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 13.5%;\r\n    top: 632px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p13[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 78.5%;\r\n    top: 1272px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p14[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 66.5%;\r\n    top: 1272px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p15[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 54.5%;\r\n    top: 1272px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p16[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 42.5%;\r\n    top: 1272px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p17[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 30.5%;\r\n    top: 1272px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p18[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 19.5%;\r\n    top: 1272px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p19[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 72.5%;\r\n    top: 1840px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p20[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 60.5%;\r\n    top: 1840px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p21[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 48.5%;\r\n    top: 1840px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p22[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 36.5%;\r\n    top: 1840px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p23[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 25.5%;\r\n    top: 1840px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p24[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 13.5%;\r\n    top: 1840px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p25[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 77.5%;\r\n    top: 2484px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p26[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 65.5%;\r\n    top: 2484px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p27[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 54.5%;\r\n    top: 2484px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p28[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 42.5%;\r\n    top: 2484px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p29[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 30.5%;\r\n    top: 2484px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p30[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 18.5%;\r\n    top: 2484px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p31[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 72.5%;\r\n    top: 3064px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p32[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 60.5%;\r\n    top: 3064px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p33[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 48.5%;\r\n    top: 3064px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p34[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 36.5%;\r\n    top: 3064px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p35[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 24.5%;\r\n    top: 3064px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p36[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 12.5%;\r\n    top: 3064px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p37[_ngcontent-%COMP%]{\r\n    \r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 77.5%;\r\n    top: 3724px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n\r\n}\r\n\r\n.p38[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 66.5%;\r\n    top: 3724px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p39[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 54.5%;\r\n    top: 3724px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n\r\n}\r\n\r\n.p40[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 42.5%;\r\n    top: 3724px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p41[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 31.5%;\r\n    top: 3724px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p42[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 19.5%;\r\n    top: 3724px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p43[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 71.5%;\r\n    top: 4309px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p44[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 59.5%;\r\n    top: 4309px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p45[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 48.5%;\r\n    top: 4309px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n\r\n.p46[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 36.5%;\r\n    top: 4309px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n\r\n}\r\n\r\n.p47[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 24.5%;\r\n    top: 4309px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n\r\n}\r\n\r\n.p48[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 10.5%;\r\n    height: 549px;\r\n    right: 13.5%;\r\n    top: 4309px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9waG9uZS1pbWcvcGhvbmUtaW1nLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxXQUFXO0lBQ1gsb0JBQW9CO0lBQ3BCLDJCQUEyQixFQUFFLHFCQUFxQjtJQUNsRCw0QkFBNEIsRUFBRSw0QkFBNEI7SUFDMUQsc0JBQXNCLEVBQUUsOERBQThEO0FBQzFGOztBQUVBO0lBQ0ksV0FBVztJQUNYLG9CQUFvQjtJQUNwQiwyQkFBMkIsRUFBRSxxQkFBcUI7SUFDbEQsNEJBQTRCLEVBQUUsNEJBQTRCO0lBQzFELHNCQUFzQixFQUFFLDhEQUE4RDtBQUMxRjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osU0FBUzs7SUFFVCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsV0FBVztJQUNYLFNBQVM7O0lBRVQsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixTQUFTOztJQUVULG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osU0FBUzs7SUFFVCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsV0FBVztJQUNYLFNBQVM7O0lBRVQsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixTQUFTOztJQUVULG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osVUFBVTs7SUFFVixtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsV0FBVztJQUNYLFVBQVU7O0lBRVYsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixXQUFXO0lBQ1gsVUFBVTs7SUFFVixtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFVBQVU7O0lBRVYsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTs7SUFFSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZOztBQUVoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZOztBQUVoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7O0FBRWhCOztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTs7QUFFaEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9waG9uZS1pbWcvcGhvbmUtaW1nLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbi1ob21le1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICAvKiBoZWlnaHQ6IDUwMDBweDsgKi9cclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjsgLyogQ2VudGVyIHRoZSBpbWFnZSAqL1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDsgLyogRG8gbm90IHJlcGVhdCB0aGUgaW1hZ2UgKi9cclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7IC8qIFJlc2l6ZSB0aGUgYmFja2dyb3VuZCBpbWFnZSB0byBjb3ZlciB0aGUgZW50aXJlIGNvbnRhaW5lciAqL1xyXG59XHJcblxyXG4ubWFpbi1ob21le1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICAvKiBoZWlnaHQ6IDUwMDBweDsgKi9cclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjsgLyogQ2VudGVyIHRoZSBpbWFnZSAqL1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDsgLyogRG8gbm90IHJlcGVhdCB0aGUgaW1hZ2UgKi9cclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7IC8qIFJlc2l6ZSB0aGUgYmFja2dyb3VuZCBpbWFnZSB0byBjb3ZlciB0aGUgZW50aXJlIGNvbnRhaW5lciAqL1xyXG59XHJcbi5wMXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiA3OC41JTtcclxuICAgIHRvcDogNTlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6NjYuNSU7XHJcbiAgICB0b3A6IDU5cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wM3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiA1NC41JTtcclxuICAgIHRvcDogNTlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDQyLjUlO1xyXG4gICAgdG9wOiA1OXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDozMC41JTtcclxuICAgIHRvcDogNTlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA2e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDE4LjUlO1xyXG4gICAgdG9wOiA1OXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDd7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogNzIuNSU7XHJcbiAgICB0b3A6IDYzMnB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDh7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDo2MC41JTtcclxuICAgIHRvcDogNjMycHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wOXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiA0OC41JTtcclxuICAgIHRvcDogNjMycHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMTB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDozNi41JTtcclxuICAgIHRvcDogNjMycHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMTF7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogMjQuNSU7XHJcbiAgICB0b3A6IDYzMnB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDEye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDEzLjUlO1xyXG4gICAgdG9wOiA2MzJweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAxM3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiA3OC41JTtcclxuICAgIHRvcDogMTI3MnB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDE0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDY2LjUlO1xyXG4gICAgdG9wOiAxMjcycHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMTV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogNTQuNSU7XHJcbiAgICB0b3A6IDEyNzJweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAxNntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiA0Mi41JTtcclxuICAgIHRvcDogMTI3MnB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDE3e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDMwLjUlO1xyXG4gICAgdG9wOiAxMjcycHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMTh7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogMTkuNSU7XHJcbiAgICB0b3A6IDEyNzJweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAxOXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiA3Mi41JTtcclxuICAgIHRvcDogMTg0MHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDIwe1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDYwLjUlO1xyXG4gICAgdG9wOiAxODQwcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMjF7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogNDguNSU7XHJcbiAgICB0b3A6IDE4NDBweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAyMntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiAzNi41JTtcclxuICAgIHRvcDogMTg0MHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDIze1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDI1LjUlO1xyXG4gICAgdG9wOiAxODQwcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMjR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogMTMuNSU7XHJcbiAgICB0b3A6IDE4NDBweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAyNXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiA3Ny41JTtcclxuICAgIHRvcDogMjQ4NHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDI2e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDY1LjUlO1xyXG4gICAgdG9wOiAyNDg0cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMjd7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogNTQuNSU7XHJcbiAgICB0b3A6IDI0ODRweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAyOHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiA0Mi41JTtcclxuICAgIHRvcDogMjQ4NHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDI5e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDMwLjUlO1xyXG4gICAgdG9wOiAyNDg0cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMzB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogMTguNSU7XHJcbiAgICB0b3A6IDI0ODRweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAzMXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiA3Mi41JTtcclxuICAgIHRvcDogMzA2NHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDMye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDYwLjUlO1xyXG4gICAgdG9wOiAzMDY0cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMzN7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogNDguNSU7XHJcbiAgICB0b3A6IDMwNjRweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAzNHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiAzNi41JTtcclxuICAgIHRvcDogMzA2NHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDM1e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDI0LjUlO1xyXG4gICAgdG9wOiAzMDY0cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMzZ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogMTIuNSU7XHJcbiAgICB0b3A6IDMwNjRweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAzN3tcclxuICAgIFxyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDc3LjUlO1xyXG4gICAgdG9wOiAzNzI0cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG5cclxufVxyXG4ucDM4e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDY2LjUlO1xyXG4gICAgdG9wOiAzNzI0cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMzl7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogNTQuNSU7XHJcbiAgICB0b3A6IDM3MjRweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcblxyXG59XHJcbi5wNDB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogNDIuNSU7XHJcbiAgICB0b3A6IDM3MjRweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA0MXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiAzMS41JTtcclxuICAgIHRvcDogMzcyNHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDQye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDE5LjUlO1xyXG4gICAgdG9wOiAzNzI0cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNDN7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogNzEuNSU7XHJcbiAgICB0b3A6IDQzMDlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA0NHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDEwLjUlO1xyXG4gICAgaGVpZ2h0OiA1NDlweDtcclxuICAgIHJpZ2h0OiA1OS41JTtcclxuICAgIHRvcDogNDMwOXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDQ1e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTAuNSU7XHJcbiAgICBoZWlnaHQ6IDU0OXB4O1xyXG4gICAgcmlnaHQ6IDQ4LjUlO1xyXG4gICAgdG9wOiA0MzA5cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNDZ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogMzYuNSU7XHJcbiAgICB0b3A6IDQzMDlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcblxyXG59XHJcbi5wNDd7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogMjQuNSU7XHJcbiAgICB0b3A6IDQzMDlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcblxyXG59XHJcbi5wNDh7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMC41JTtcclxuICAgIGhlaWdodDogNTQ5cHg7XHJcbiAgICByaWdodDogMTMuNSU7XHJcbiAgICB0b3A6IDQzMDlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PhoneImgComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-phone-img',
                templateUrl: './phone-img.component.html',
                styleUrls: ['./phone-img.component.css']
            }]
    }], function () { return [{ type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] }, { type: _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__["LoadingBarService"] }]; }, { init: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ["init"]
        }] }); })();


/***/ }),

/***/ "./src/app/components/plataforma-pago/modal-pago/modal-pago.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/plataforma-pago/modal-pago/modal-pago.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ModalPagoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalPagoComponent", function() { return ModalPagoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_idioma_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/idioma.service */ "./src/app/services/idioma.service.ts");
/* harmony import */ var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-loading-bar/core */ "./node_modules/@ngx-loading-bar/core/__ivy_ngcc__/fesm2015/ngx-loading-bar-core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var ngx_paypal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-paypal */ "./node_modules/ngx-paypal/__ivy_ngcc__/fesm2015/ngx-paypal.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");










function ModalPagoComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "ngx-paypal", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("config", ctx_r0.payPalConfig);
} }
const _c0 = function (a0) { return [a0]; };
const _c1 = function () { return ["../exclusivo"]; };
function ModalPagoComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ModalPagoComponent_div_1_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r2.loadingBarService.start(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](2, _c0, "assets/popup-material-exclusivo/" + ctx_r1.idioma.idioma + ".png"), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c1));
} }
class ModalPagoComponent {
    constructor(fb, idioma, loadingBarService) {
        this.fb = fb;
        this.idioma = idioma;
        this.loadingBarService = loadingBarService;
        this.background = "./assets/formulario-pago/";
        this.amountDonate = localStorage.getItem("donacion");
        this.seeForm = false;
        this.materialExclusivo = false;
        this.form = this.fb.group({
            correo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            monto: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
        this.initConfig();
        this.background = `${this.background}${idioma.idioma}.png`;
    }
    donar() {
        console.log(this.form.value);
        this.seeForm = false;
        this.amountDonate = this.form.value.monto;
    }
    initConfig() {
        this.payPalConfig = {
            currency: 'USD',
            clientId: 'sb',
            createOrderOnClient: (data) => ({
                intent: 'CAPTURE',
                purchase_units: [
                    {
                        amount: {
                            currency_code: 'USD',
                            value: this.amountDonate,
                            breakdown: {
                                item_total: {
                                    currency_code: 'USD',
                                    value: this.amountDonate
                                }
                            }
                        },
                        items: [
                            {
                                name: 'Donación Digital Halsey',
                                quantity: '1',
                                category: 'DIGITAL_GOODS',
                                unit_amount: {
                                    currency_code: 'USD',
                                    value: this.amountDonate,
                                },
                            }
                        ]
                    }
                ]
            }),
            advanced: {
                commit: 'true'
            },
            style: {
                label: 'paypal',
                layout: 'vertical',
            },
            onApprove: (data, actions) => {
                console.log('onApprove - transaction was approved, but not authorized', data, actions);
                actions.order.get().then(details => {
                    console.log('onApprove - you can get full order details inside onApprove: ', details);
                    this.materialExclusivo = true;
                });
            },
            onClientAuthorization: (data) => {
                console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
            },
            onCancel: (data, actions) => {
                console.log('OnCancel', data, actions);
            },
            onError: err => {
                console.log('OnError', err);
            },
            onClick: (data, actions) => {
                console.log('onClick', data, actions);
            },
        };
    }
}
ModalPagoComponent.ɵfac = function ModalPagoComponent_Factory(t) { return new (t || ModalPagoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_idioma_service__WEBPACK_IMPORTED_MODULE_2__["IdiomasLanding"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__["LoadingBarService"])); };
ModalPagoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ModalPagoComponent, selectors: [["app-modal-pago"]], decls: 2, vars: 2, consts: [["style", "height: 400px;", 4, "ngIf"], [4, "ngIf"], [2, "height", "400px"], [3, "config"], [2, "height", "400px", 3, "src"], ["mat-dialog-close", "", "routerLinkActive", "router-link-active", 1, "button-invisible", 3, "routerLink", "click"]], template: function ModalPagoComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, ModalPagoComponent_div_0_Template, 2, 1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ModalPagoComponent_div_1_Template, 3, 5, "div", 1);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.materialExclusivo);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.materialExclusivo);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], ngx_paypal__WEBPACK_IMPORTED_MODULE_5__["NgxPaypalComponent"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialogClose"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterLinkActive"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterLink"]], styles: [".mat-focused .mat-form-field-label {\r\n    \r\n    color: white !important;\r\n   }\r\n  \r\n    .mat-form-field-underline {\r\n    \r\n    background-color: white !important;\r\n  }\r\n  \r\n    .mat-form-field-ripple {\r\n   \r\n   background-color: white !important;;\r\n  }\r\n  \r\n     .mat-form-field-empty.mat-form-field-label {\r\n    color: white;\r\n}\r\n  \r\n     .mat-form-field.mat-form-field-invalid .mat-form-field-label {\r\n  color: #f44336;\r\n  font-size: 20px !important;\r\n\r\n}\r\n  \r\n   .button-invisible[_ngcontent-%COMP%]{\r\n  background: transparent;\r\n  width: 41%;\r\n  height: 87px;\r\n  position: absolute;\r\n  left: 52%;\r\n  top: 399px;\r\n  outline: none;\r\n  border: none;\r\n}\r\n  \r\n   \r\n  \r\n   .img[_ngcontent-%COMP%]{\r\n  position: relative;\r\n  width: 100%;\r\n  \r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wbGF0YWZvcm1hLXBhZ28vbW9kYWwtcGFnby9tb2RhbC1wYWdvLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSx3QkFBd0I7SUFDeEIsdUJBQXVCO0dBQ3hCOztHQUVBO0lBQ0MsNEJBQTRCO0lBQzVCLGtDQUFrQztFQUNwQzs7R0FFQTtHQUNDLHlDQUF5QztHQUN6QyxrQ0FBa0M7RUFDbkM7O0dBQ0E7SUFDRSxZQUFZO0FBQ2hCOztHQUVBO0VBQ0UsY0FBYztFQUNkLDBCQUEwQjs7QUFFNUI7O0dBR0E7RUFDRSx1QkFBdUI7RUFDdkIsVUFBVTtFQUNWLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsU0FBUztFQUNULFVBQVU7RUFDVixhQUFhO0VBQ2IsWUFBWTtBQUNkOztHQUVBOzs7R0FHRzs7R0FDSDtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsaUJBQWlCO0FBQ25CIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9wbGF0YWZvcm1hLXBhZ28vbW9kYWwtcGFnby9tb2RhbC1wYWdvLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6Om5nLWRlZXAgLm1hdC1mb2N1c2VkIC5tYXQtZm9ybS1maWVsZC1sYWJlbCB7XHJcbiAgICAvKmNoYW5nZSBjb2xvciBvZiBsYWJlbCovXHJcbiAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcclxuICAgfVxyXG4gIFxyXG4gICA6Om5nLWRlZXAubWF0LWZvcm0tZmllbGQtdW5kZXJsaW5lIHtcclxuICAgIC8qY2hhbmdlIGNvbG9yIG9mIHVuZGVybGluZSovXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gIH0gXHJcbiAgXHJcbiAgOjpuZy1kZWVwLm1hdC1mb3JtLWZpZWxkLXJpcHBsZSB7XHJcbiAgIC8qY2hhbmdlIGNvbG9yIG9mIHVuZGVybGluZSB3aGVuIGZvY3VzZWQqL1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50OztcclxuICB9XHJcbiAgOjpuZy1kZWVwIC5tYXQtZm9ybS1maWVsZC1lbXB0eS5tYXQtZm9ybS1maWVsZC1sYWJlbCB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LWZvcm0tZmllbGQubWF0LWZvcm0tZmllbGQtaW52YWxpZCAubWF0LWZvcm0tZmllbGQtbGFiZWwge1xyXG4gIGNvbG9yOiAjZjQ0MzM2O1xyXG4gIGZvbnQtc2l6ZTogMjBweCAhaW1wb3J0YW50O1xyXG5cclxufVxyXG5cclxuXHJcbi5idXR0b24taW52aXNpYmxle1xyXG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gIHdpZHRoOiA0MSU7XHJcbiAgaGVpZ2h0OiA4N3B4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBsZWZ0OiA1MiU7XHJcbiAgdG9wOiAzOTlweDtcclxuICBvdXRsaW5lOiBub25lO1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG5cclxuLyogLmVtYWlse1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBtYXJnaW4tcmlnaHQ6IDUwcHg7XHJcbn0gKi9cclxuLmltZ3tcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgLyogaGVpZ2h0OiA0MCU7ICovXHJcbn1cclxuXHJcbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ModalPagoComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-modal-pago',
                templateUrl: './modal-pago.component.html',
                styleUrls: ['./modal-pago.component.css']
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: _services_idioma_service__WEBPACK_IMPORTED_MODULE_2__["IdiomasLanding"] }, { type: _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__["LoadingBarService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/components/plataforma-pago/plataforma-pago.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/plataforma-pago/plataforma-pago.component.ts ***!
  \*************************************************************************/
/*! exports provided: PlataformaPagoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlataformaPagoComponent", function() { return PlataformaPagoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _modal_pago_modal_pago_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal-pago/modal-pago.component */ "./src/app/components/plataforma-pago/modal-pago/modal-pago.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_idioma_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/idioma.service */ "./src/app/services/idioma.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-loading-bar/core */ "./node_modules/@ngx-loading-bar/core/__ivy_ngcc__/fesm2015/ngx-loading-bar-core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");










function PlataformaPagoComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "input", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "input", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PlataformaPagoComponent_div_2_Template_button_click_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r2.donar(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx_r0.background2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
const _c0 = function (a0) { return [a0]; };
const _c1 = function () { return ["../exclusivo"]; };
function PlataformaPagoComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PlataformaPagoComponent_div_4_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4.loadingBarService.start(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](2, _c0, "assets/popup-material-exclusivo/" + ctx_r1.idioma.idioma + ".png"), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c1));
} }
class PlataformaPagoComponent {
    constructor(idiomas, fb, dialog, loadingBarService) {
        this.idiomas = idiomas;
        this.fb = fb;
        this.dialog = dialog;
        this.loadingBarService = loadingBarService;
        this.oculter = true;
        this.background2 = "./assets/formulario-pago/";
        this.seeForm = true;
        this.materialExclusivo = false;
        this.form = this.fb.group({
            correo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            monto: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
        this.background = `assets/cabina-pago/${idiomas.idioma}.jpg`;
        this.background2 = `${this.background2}${idiomas.idioma}.png`;
        const muestraWidth = () => {
            setTimeout(() => {
                const x = document.getElementById("background-img").getBoundingClientRect().width;
                console.log(x);
                const modal = document.getElementById("background2");
                modal.style.top = `${114 * x / 1125}px`;
                modal.style.left = `${600 * x / 1125}px`;
                const email = document.getElementById("email");
                email.style.top = `${147 * x / 1125}px`;
                email.style.left = `${619 * x / 1125}px`;
                email.style.width = `${302 * x / 1125}px`;
                email.style.height = `${23 * x / 1125}px`;
                const monto = document.getElementById("monto");
                monto.style.top = `${232 * x / 1125}px`;
                monto.style.left = `${648 * x / 1125}px`;
                monto.style.width = `${273 * x / 1125}px`;
                monto.style.height = `${23 * x / 1125}px`;
                const donar = document.getElementById("donara");
                donar.style.top = `${284 * x / 1125}px`;
                donar.style.left = `${618 * x / 1125}px`;
                donar.style.width = `${210 * x / 1125}px`;
                donar.style.left = `${620 * x / 1125}px`;
                donar.style.height = `${28 * x / 1125}px`;
                // const element = document.getElementById('desktopImg')
                // element.style.top = `${1000*x/1662}px`
                // const element2 = document.getElementById('phoneImg')
                // element2.style.top = `${1959*x/1662}px`
                // const element3 = document.getElementById('smallphoneImg')
                // element3.style.top = `${2612*x/1662}px`
                // const element4 = document.getElementById('banderas')
                // element4.style.height = `${644*x/1662}px`
                muestraWidth();
            }, 400);
        };
        muestraWidth();
        // const dialogRef = this.dialog.open(ModalPagoComponent, {
        //   width: '40%',
        //   panelClass: 'modal-pago',
        //   disableClose: true,
        //   position: {top: '80px', left:'55%'}
        // });
    }
    pagar() {
        const dialogRef = this.dialog.open(_modal_pago_modal_pago_component__WEBPACK_IMPORTED_MODULE_1__["ModalPagoComponent"], {
            width: '30%',
            position: { top: '160px', left: '500px' }
        });
        // ,{ top: '50px', left: '50px' }
    }
    doSomething() {
        // const dialogRef = this.dialog.open(DescargaComponent, {
        //   width: '40%'
        // });
        this.loadingBarService.complete();
    }
    donar() {
        console.log(this.form.value.monto);
        if (this.form.value.monto !== "" && !isNaN(this.form.value.monto)) {
            localStorage.setItem("donacion", this.form.value.monto);
            const dialogRef = this.dialog.open(_modal_pago_modal_pago_component__WEBPACK_IMPORTED_MODULE_1__["ModalPagoComponent"], {
                width: '35%',
                panelClass: 'modal-pago',
                disableClose: true,
                position: { top: '80px', left: '55%' }
            });
            dialogRef.afterClosed().subscribe(() => {
                this.oculter = true;
            });
            this.oculter = false;
        }
        else {
            alert("El monto ingresado no es valido");
        }
        // console.log(this.form.value);
        // this.seeForm=false;
        // this.amountDonate= this.form.value.monto
        // this.initConfig();
    }
    initConfig() {
        this.payPalConfig = {
            currency: 'USD',
            clientId: 'sb',
            createOrderOnClient: (data) => ({
                intent: 'CAPTURE',
                purchase_units: [
                    {
                        amount: {
                            currency_code: 'USD',
                            value: this.amountDonate,
                            breakdown: {
                                item_total: {
                                    currency_code: 'USD',
                                    value: this.amountDonate
                                }
                            }
                        },
                        items: [
                            {
                                name: 'Donación Digital Halsey',
                                quantity: '1',
                                category: 'DIGITAL_GOODS',
                                unit_amount: {
                                    currency_code: 'USD',
                                    value: this.amountDonate,
                                },
                            }
                        ]
                    }
                ]
            }),
            advanced: {
                commit: 'true'
            },
            style: {
                label: 'paypal',
                layout: 'vertical',
            },
            onApprove: (data, actions) => {
                console.log('onApprove - transaction was approved, but not authorized', data, actions);
                actions.order.get().then(details => {
                    console.log('onApprove - you can get full order details inside onApprove: ', details);
                    this.materialExclusivo = true;
                });
            },
            onClientAuthorization: (data) => {
                console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
            },
            onCancel: (data, actions) => {
                console.log('OnCancel', data, actions);
            },
            onError: err => {
                console.log('OnError', err);
            },
            onClick: (data, actions) => {
                console.log('onClick', data, actions);
            },
        };
    }
}
PlataformaPagoComponent.ɵfac = function PlataformaPagoComponent_Factory(t) { return new (t || PlataformaPagoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_idioma_service__WEBPACK_IMPORTED_MODULE_3__["IdiomasLanding"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_5__["LoadingBarService"])); };
PlataformaPagoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PlataformaPagoComponent, selectors: [["app-plataforma-pago"]], decls: 5, vars: 4, consts: [["id", "background-img", 1, "main-home", 3, "src", "load"], [3, "formGroup"], ["class", "row text-center", 4, "ngIf"], [4, "ngIf"], [1, "row", "text-center"], [1, "col-12"], ["id", "background2", "alt", "", 1, "background2", 3, "src"], ["type", "text", "id", "email", "formControlName", "correo", 2, "position", "absolute", "background-position", "center", "top", "147px", "left", "619px", "width", "302px", "height", "23px"], ["type", "text", "id", "monto", "formControlName", "monto", 2, "position", "absolute", "background-position", "center", "top", "232px", "left", "648px", "width", "273px", "border", "0px", "height", "23px"], ["id", "donara", 1, "donara", 3, "click"], [2, "height", "290px", 3, "src"], ["mat-dialog-close", "", "routerLinkActive", "router-link-active", 1, "button-invisible", 3, "routerLink", "click"]], template: function PlataformaPagoComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "img", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("load", function PlataformaPagoComponent_Template_img_load_0_listener() { return ctx.doSomething(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "form", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, PlataformaPagoComponent_div_2_Template, 7, 1, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, PlataformaPagoComponent_div_4_Template, 3, 5, "div", 3);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.background, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.oculter);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.materialExclusivo);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroupDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControlName"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogClose"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterLinkActive"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterLink"]], styles: [".main-home[_ngcontent-%COMP%]{\r\n    width: 100%;    \r\n    \r\n    background-position: center; \r\n    background-repeat: no-repeat; \r\n    background-size: cover; \r\n}\r\n.paypal[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 195px;\r\n    right: 67.5%;\r\n    top: 187px;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.credit[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 12.5%;\r\n    height: 195px;\r\n    right: 49.5%;\r\n    top: 187px;\r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.button-invisible[_ngcontent-%COMP%]{\r\n    background: transparent;\r\n    width: 41%;\r\n    height: 30px;\r\n    position: absolute;\r\n    margin-left: lef;\r\n    left: 52%;\r\n    top: 413px;\r\n    outline: none;\r\n    border:none;\r\n  }\r\n\r\n.img[_ngcontent-%COMP%]{\r\n    position: relative;\r\n    width: 100%;\r\n    \r\n  }\r\n.background2[_ngcontent-%COMP%]{\r\n    width: 30%;\r\n    position: absolute;\r\n    top: 114px;\r\n    left: 600px;\r\n  }\r\n.donara[_ngcontent-%COMP%]{\r\n    background: transparent;\r\n    width: 21%;\r\n    height: 30px;\r\n    position: absolute;\r\n    left: 618ox;\r\n    top: 284px;\r\n    outline: none;\r\n    border: none;\r\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wbGF0YWZvcm1hLXBhZ28vcGxhdGFmb3JtYS1wYWdvLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLDJCQUEyQixFQUFFLHFCQUFxQjtJQUNsRCw0QkFBNEIsRUFBRSw0QkFBNEI7SUFDMUQsc0JBQXNCLEVBQUUsOERBQThEO0FBQzFGO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFVBQVU7SUFDVixtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osVUFBVTtJQUNWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUlBO0lBQ0ksdUJBQXVCO0lBQ3ZCLFVBQVU7SUFDVixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixTQUFTO0lBQ1QsVUFBVTtJQUNWLGFBQWE7SUFDYixXQUFXO0VBQ2I7QUFFQTs7O0tBR0c7QUFDSDtJQUNFLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsaUJBQWlCO0VBQ25CO0FBRUE7SUFDRSxVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixXQUFXO0VBQ2I7QUFFQTtJQUNFLHVCQUF1QjtJQUN2QixVQUFVO0lBQ1YsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsVUFBVTtJQUNWLGFBQWE7SUFDYixZQUFZO0VBQ2QiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3BsYXRhZm9ybWEtcGFnby9wbGF0YWZvcm1hLXBhZ28uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluLWhvbWV7XHJcbiAgICB3aWR0aDogMTAwJTsgICAgXHJcbiAgICAvKiBoZWlnaHQ6IDcwMHB4OyAqL1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyOyAvKiBDZW50ZXIgdGhlIGltYWdlICovXHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0OyAvKiBEbyBub3QgcmVwZWF0IHRoZSBpbWFnZSAqL1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjsgLyogUmVzaXplIHRoZSBiYWNrZ3JvdW5kIGltYWdlIHRvIGNvdmVyIHRoZSBlbnRpcmUgY29udGFpbmVyICovXHJcbn1cclxuLnBheXBhbHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAxOTVweDtcclxuICAgIHJpZ2h0OiA2Ny41JTtcclxuICAgIHRvcDogMTg3cHg7XHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4uY3JlZGl0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTIuNSU7XHJcbiAgICBoZWlnaHQ6IDE5NXB4O1xyXG4gICAgcmlnaHQ6IDQ5LjUlO1xyXG4gICAgdG9wOiAxODdweDtcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcblxyXG5cclxuXHJcbi5idXR0b24taW52aXNpYmxle1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogNDElO1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IGxlZjtcclxuICAgIGxlZnQ6IDUyJTtcclxuICAgIHRvcDogNDEzcHg7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgYm9yZGVyOm5vbmU7XHJcbiAgfVxyXG4gIFxyXG4gIC8qIC5lbWFpbHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIG1hcmdpbi1yaWdodDogNTBweDtcclxuICB9ICovXHJcbiAgLmltZ3tcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLyogaGVpZ2h0OiA0MCU7ICovXHJcbiAgfVxyXG5cclxuICAuYmFja2dyb3VuZDJ7XHJcbiAgICB3aWR0aDogMzAlO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAxMTRweDtcclxuICAgIGxlZnQ6IDYwMHB4O1xyXG4gIH1cclxuICBcclxuICAuZG9uYXJhe1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMjElO1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogNjE4b3g7XHJcbiAgICB0b3A6IDI4NHB4O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICB9XHJcbiAgIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PlataformaPagoComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-plataforma-pago',
                templateUrl: './plataforma-pago.component.html',
                styleUrls: ['./plataforma-pago.component.css']
            }]
    }], function () { return [{ type: _services_idioma_service__WEBPACK_IMPORTED_MODULE_3__["IdiomasLanding"] }, { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }, { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] }, { type: _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_5__["LoadingBarService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/components/smallphone-img/smallphone-img.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/smallphone-img/smallphone-img.component.ts ***!
  \***********************************************************************/
/*! exports provided: SmallphoneImgComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmallphoneImgComponent", function() { return SmallphoneImgComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _modal_modal_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../modal/modal.component */ "./src/app/components/modal/modal.component.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-loading-bar/core */ "./node_modules/@ngx-loading-bar/core/__ivy_ngcc__/fesm2015/ngx-loading-bar-core.js");





const _c0 = ["init"];
class SmallphoneImgComponent {
    constructor(dialog, loadingBarService) {
        this.dialog = dialog;
        this.loadingBarService = loadingBarService;
        const muestraWidth = () => {
            setTimeout(() => {
                const x = document.getElementById("background-img").getBoundingClientRect().width;
                console.log(x);
                const elements = ['p1', 'p2', 'p3', 'p4', 'p5', 'p6'];
                const elements2 = ['p7', 'p8', 'p9', 'p10', 'p11', 'p12'];
                const elements3 = ['p13', 'p14', 'p15', 'p16', 'p17', 'p18'];
                const elements4 = ['p19', 'p20', 'p21', 'p22', 'p23', 'p24'];
                const elements5 = ['p25', 'p26', 'p27', 'p28', 'p29', 'p30'];
                const elements6 = ['p31', 'p32', 'p33', 'p34', 'p35', 'p36'];
                const elements7 = ['p37', 'p38', 'p39', 'p40', 'p41', 'p42'];
                const elements8 = ['p43', 'p44', 'p45', 'p46', 'p47', 'p48'];
                const elements9 = ['p49', 'p50', 'p51', 'p52', 'p53', 'p54'];
                const elements10 = ['p55', 'p56', 'p57', 'p58', 'p59', 'p60'];
                const elements11 = ['p61', 'p62', 'p63', 'p64', 'p65', 'p66'];
                const elements12 = ['p67', 'p68', 'p69', 'p70', 'p71', 'p72'];
                for (let i in elements) {
                    const element = document.getElementById(elements[i]);
                    element.style.top = `${35 * x / 1108}px`;
                    element.style.height = `${234 * x / 1108}px`;
                }
                for (let i in elements2) {
                    const element = document.getElementById(elements2[i]);
                    element.style.top = `${273 * x / 1108}px`;
                    element.style.height = `${234 * x / 1108}px`;
                }
                for (let i in elements3) {
                    const element = document.getElementById(elements3[i]);
                    element.style.top = `${555 * x / 1108}px`;
                    element.style.height = `${234 * x / 1108}px`;
                }
                for (let i in elements4) {
                    const element = document.getElementById(elements4[i]);
                    element.style.top = `${802 * x / 1108}px`;
                    element.style.height = `${234 * x / 1108}px`;
                }
                for (let i in elements5) {
                    const element = document.getElementById(elements5[i]);
                    element.style.top = `${1089 * x / 1108}px`;
                    element.style.height = `${234 * x / 1108}px`;
                }
                // const elements= ['veridiomas','donacion','contacto']
                for (let i in elements6) {
                    const element = document.getElementById(elements6[i]);
                    element.style.top = `${1329 * x / 1108}px`;
                    element.style.height = `${234 * x / 1108}px`;
                }
                for (let i in elements7) {
                    const element = document.getElementById(elements7[i]);
                    element.style.top = `${1610 * x / 1108}px`;
                    element.style.height = `${234 * x / 1108}px`;
                }
                for (let i in elements8) {
                    const element = document.getElementById(elements8[i]);
                    element.style.top = `${1852 * x / 1108}px`;
                    element.style.height = `${234 * x / 1108}px`;
                }
                for (let i in elements9) {
                    const element = document.getElementById(elements9[i]);
                    element.style.top = `${2130 * x / 1108}px`;
                    element.style.height = `${234 * x / 1108}px`;
                }
                for (let i in elements10) {
                    const element = document.getElementById(elements10[i]);
                    element.style.top = `${2370 * x / 1108}px`;
                    element.style.height = `${234 * x / 1108}px`;
                }
                for (let i in elements11) {
                    const element = document.getElementById(elements11[i]);
                    element.style.top = `${2651 * x / 1108}px`;
                    element.style.height = `${234 * x / 1108}px`;
                }
                for (let i in elements12) {
                    const element = document.getElementById(elements12[i]);
                    element.style.top = `${2892 * x / 1108}px`;
                    element.style.height = `${234 * x / 1108}px`;
                }
                // for(let i in elements){
                //   const element = document.getElementById(elements[i])
                //   element.style.top = `${320*x/1108}px`
                // }
                // const element = document.getElementById('desktopImg')
                // element.style.top = `${1000*x/1662}px`
                // const element2 = document.getElementById('phoneImg')
                // element2.style.top = `${1959*x/1662}px`
                // const element3 = document.getElementById('smallphoneImg')
                // element3.style.top = `${2612*x/1662}px`
                // const element4 = document.getElementById('banderas')
                // element4.style.height = `${644*x/1662}px`
                muestraWidth();
            }, 400);
        };
        muestraWidth();
        setTimeout(() => {
            this.init.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
        }, 500);
    }
    openDialog(link) {
        console.log(link);
        const dialogRef = this.dialog.open(_modal_modal_component__WEBPACK_IMPORTED_MODULE_1__["ModalComponent"], {
            width: '45%',
            data: link,
            position: { top: '10px' }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }
    doSomething() {
        // const dialogRef = this.dialog.open(DescargaComponent, {
        //   width: '40%'
        // });
        this.loadingBarService.complete();
    }
}
SmallphoneImgComponent.ɵfac = function SmallphoneImgComponent_Factory(t) { return new (t || SmallphoneImgComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__["LoadingBarService"])); };
SmallphoneImgComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: SmallphoneImgComponent, selectors: [["app-smallphone-img"]], viewQuery: function SmallphoneImgComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
    } if (rf & 2) {
        var _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.init = _t.first);
    } }, decls: 75, vars: 0, consts: [[2, "top", "0"], ["init", ""], ["src", "assets/smallphoneImg/list.jpg", "id", "background-img", 1, "main-home", 3, "load"], ["id", "p1", 1, "p1", 3, "click"], ["id", "p2", 1, "p2", 3, "click"], ["id", "p3", 1, "p3", 3, "click"], ["id", "p4", 1, "p4", 3, "click"], ["id", "p5", 1, "p5", 3, "click"], ["id", "p6", 1, "p6", 3, "click"], ["id", "p7", 1, "p7", 3, "click"], ["id", "p8", 1, "p8", 3, "click"], ["id", "p9", 1, "p9", 3, "click"], ["id", "p10", 1, "p10", 3, "click"], ["id", "p11", 1, "p11", 3, "click"], ["id", "p12", 1, "p12", 3, "click"], ["id", "p13", 1, "p13", 3, "click"], ["id", "p14", 1, "p14", 3, "click"], ["id", "p15", 1, "p15", 3, "click"], ["id", "p16", 1, "p16", 3, "click"], ["id", "p17", 1, "p17", 3, "click"], ["id", "p18", 1, "p18", 3, "click"], ["id", "p19", 1, "p19", 3, "click"], ["id", "p20", 1, "p20", 3, "click"], ["id", "p21", 1, "p21", 3, "click"], ["id", "p22", 1, "p22", 3, "click"], ["id", "p23", 1, "p23", 3, "click"], ["id", "p24", 1, "p24", 3, "click"], ["id", "p25", 1, "p25", 3, "click"], ["id", "p26", 1, "p26", 3, "click"], ["id", "p27", 1, "p27", 3, "click"], ["id", "p28", 1, "p28", 3, "click"], ["id", "p29", 1, "p29", 3, "click"], ["id", "p30", 1, "p30", 3, "click"], ["id", "p31", 1, "p31", 3, "click"], ["id", "p32", 1, "p32", 3, "click"], ["id", "p33", 1, "p33", 3, "click"], ["id", "p34", 1, "p34", 3, "click"], ["id", "p35", 1, "p35", 3, "click"], ["id", "p36", 1, "p36", 3, "click"], ["id", "p37", 1, "p37", 3, "click"], ["id", "p38", 1, "p38", 3, "click"], ["id", "p39", 1, "p39", 3, "click"], ["id", "p40", 1, "p40", 3, "click"], ["id", "p41", 1, "p41", 3, "click"], ["id", "p42", 1, "p42", 3, "click"], ["id", "p43", 1, "p43", 3, "click"], ["id", "p44", 1, "p44", 3, "click"], ["id", "p45", 1, "p45", 3, "click"], ["id", "p46", 1, "p46", 3, "click"], ["id", "p47", 1, "p47", 3, "click"], ["id", "p48", 1, "p48", 3, "click"], ["id", "p49", 1, "p49", 3, "click"], ["id", "p50", 1, "p50", 3, "click"], ["id", "p51", 1, "p51", 3, "click"], ["id", "p52", 1, "p52", 3, "click"], ["id", "p53", 1, "p53", 3, "click"], ["id", "p54", 1, "p54", 3, "click"], ["id", "p55", 1, "p55", 3, "click"], ["id", "p56", 1, "p56", 3, "click"], ["id", "p57", 1, "p57", 3, "click"], ["id", "p58", 1, "p58", 3, "click"], ["id", "p59", 1, "p59", 3, "click"], ["id", "p60", 1, "p60", 3, "click"], ["id", "p61", 1, "p61", 3, "click"], ["id", "p62", 1, "p62", 3, "click"], ["id", "p63", 1, "p63", 3, "click"], ["id", "p64", 1, "p64", 3, "click"], ["id", "p65", 1, "p65", 3, "click"], ["id", "p66", 1, "p66", 3, "click"], ["id", "p67", 1, "p67", 3, "click"], ["id", "p68", 1, "p68", 3, "click"], ["id", "p69", 1, "p69", 3, "click"], ["id", "p70", 1, "p70", 3, "click"], ["id", "p71", 1, "p71", 3, "click"], ["id", "p72", 1, "p72", 3, "click"]], template: function SmallphoneImgComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 0, 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("load", function SmallphoneImgComponent_Template_img_load_2_listener() { return ctx.doSomething(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_3_listener() { return ctx.openDialog("assets/smallphoneImg/1MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_4_listener() { return ctx.openDialog("assets/smallphoneImg/2MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_5_listener() { return ctx.openDialog("assets/smallphoneImg/3MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_6_listener() { return ctx.openDialog("assets/smallphoneImg/4MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_7_listener() { return ctx.openDialog("assets/smallphoneImg/5MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_8_listener() { return ctx.openDialog("assets/smallphoneImg/6MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_9_listener() { return ctx.openDialog("assets/smallphoneImg/7MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_10_listener() { return ctx.openDialog("assets/smallphoneImg/8MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_11_listener() { return ctx.openDialog("assets/smallphoneImg/9MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_12_listener() { return ctx.openDialog("assets/smallphoneImg/10MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_13_listener() { return ctx.openDialog("assets/smallphoneImg/11MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_14_listener() { return ctx.openDialog("assets/smallphoneImg/12MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_15_listener() { return ctx.openDialog("assets/smallphoneImg/13MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "button", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_16_listener() { return ctx.openDialog("assets/smallphoneImg/14MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_17_listener() { return ctx.openDialog("assets/smallphoneImg/15MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_18_listener() { return ctx.openDialog("assets/smallphoneImg/16MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "button", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_19_listener() { return ctx.openDialog("assets/smallphoneImg/17MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_20_listener() { return ctx.openDialog("assets/smallphoneImg/18MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_21_listener() { return ctx.openDialog("assets/smallphoneImg/19MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_22_listener() { return ctx.openDialog("assets/smallphoneImg/20MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "button", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_23_listener() { return ctx.openDialog("assets/smallphoneImg/21MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "button", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_24_listener() { return ctx.openDialog("assets/smallphoneImg/22MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "button", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_25_listener() { return ctx.openDialog("assets/smallphoneImg/23MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "button", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_26_listener() { return ctx.openDialog("assets/smallphoneImg/24MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "button", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_27_listener() { return ctx.openDialog("assets/smallphoneImg/25MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "button", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_28_listener() { return ctx.openDialog("assets/smallphoneImg/26MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "button", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_29_listener() { return ctx.openDialog("assets/smallphoneImg/27MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "button", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_30_listener() { return ctx.openDialog("assets/smallphoneImg/28MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "button", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_31_listener() { return ctx.openDialog("assets/smallphoneImg/29MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "button", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_32_listener() { return ctx.openDialog("assets/smallphoneImg/30MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "button", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_33_listener() { return ctx.openDialog("assets/smallphoneImg/31MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "button", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_34_listener() { return ctx.openDialog("assets/smallphoneImg/32MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "button", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_35_listener() { return ctx.openDialog("assets/smallphoneImg/33MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "button", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_36_listener() { return ctx.openDialog("assets/smallphoneImg/34MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "button", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_37_listener() { return ctx.openDialog("assets/smallphoneImg/35MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "button", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_38_listener() { return ctx.openDialog("assets/smallphoneImg/36MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "button", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_39_listener() { return ctx.openDialog("assets/smallphoneImg/37MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "button", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_40_listener() { return ctx.openDialog("assets/smallphoneImg/38MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "button", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_41_listener() { return ctx.openDialog("assets/smallphoneImg/39MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "button", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_42_listener() { return ctx.openDialog("assets/smallphoneImg/40MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "button", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_43_listener() { return ctx.openDialog("assets/smallphoneImg/41MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "button", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_44_listener() { return ctx.openDialog("assets/smallphoneImg/42MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "button", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_45_listener() { return ctx.openDialog("assets/smallphoneImg/43MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "button", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_46_listener() { return ctx.openDialog("assets/smallphoneImg/44MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "button", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_47_listener() { return ctx.openDialog("assets/smallphoneImg/45MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "button", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_48_listener() { return ctx.openDialog("assets/smallphoneImg/46MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "button", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_49_listener() { return ctx.openDialog("assets/smallphoneImg/47MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "button", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_50_listener() { return ctx.openDialog("assets/smallphoneImg/48MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "button", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_51_listener() { return ctx.openDialog("assets/smallphoneImg/49MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "button", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_52_listener() { return ctx.openDialog("assets/smallphoneImg/50MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "button", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_53_listener() { return ctx.openDialog("assets/smallphoneImg/51MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "button", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_54_listener() { return ctx.openDialog("assets/smallphoneImg/52MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "button", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_55_listener() { return ctx.openDialog("assets/smallphoneImg/53MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "button", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_56_listener() { return ctx.openDialog("assets/smallphoneImg/54MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "button", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_57_listener() { return ctx.openDialog("assets/smallphoneImg/55MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "button", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_58_listener() { return ctx.openDialog("assets/smallphoneImg/56MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "button", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_59_listener() { return ctx.openDialog("assets/smallphoneImg/57MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "button", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_60_listener() { return ctx.openDialog("assets/smallphoneImg/58MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "button", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_61_listener() { return ctx.openDialog("assets/smallphoneImg/59MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "button", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_62_listener() { return ctx.openDialog("assets/smallphoneImg/60MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "button", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_63_listener() { return ctx.openDialog("assets/smallphoneImg/61MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "button", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_64_listener() { return ctx.openDialog("assets/smallphoneImg/62MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "button", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_65_listener() { return ctx.openDialog("assets/smallphoneImg/63MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "button", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_66_listener() { return ctx.openDialog("assets/smallphoneImg/64MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "button", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_67_listener() { return ctx.openDialog("assets/smallphoneImg/65MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "button", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_68_listener() { return ctx.openDialog("assets/smallphoneImg/66MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "button", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_69_listener() { return ctx.openDialog("assets/smallphoneImg/67MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "button", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_70_listener() { return ctx.openDialog("assets/smallphoneImg/68MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "button", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_71_listener() { return ctx.openDialog("assets/smallphoneImg/69MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "button", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_72_listener() { return ctx.openDialog("assets/smallphoneImg/70MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "button", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_73_listener() { return ctx.openDialog("assets/smallphoneImg/71MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "button", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SmallphoneImgComponent_Template_button_click_74_listener() { return ctx.openDialog("assets/smallphoneImg/72MS.jpg"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: [".main-home[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    \r\n    background-position: center; \r\n    background-repeat: no-repeat; \r\n    background-size: cover; \r\n}\r\n.p1[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 83.5%;\r\n    top: 59px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p2[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 70.5%;\r\n    top: 59px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p3[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 56.5%;\r\n    top: 59px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p4[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 42.5%;\r\n    top: 59px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p5[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 29.5%;\r\n    top: 59px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p6[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 15.5%;\r\n    top: 59px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p7[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 78.5%;\r\n    top: 429px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p8[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 64.5%;\r\n    top: 429px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p9[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 50.5%;\r\n    top: 429px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p10[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 36.5%;\r\n    top: 429px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p11[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 23.5%;\r\n    top: 429px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p12[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 9.5%;\r\n    top: 429px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p13[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 83.5%;\r\n    top: 870px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p14[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 70.5%;\r\n    top: 870px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p15[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 56.5%;\r\n    top: 870px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p16[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 42.5%;\r\n    top: 870px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p17[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 29.5%;\r\n    top: 870px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p18[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 15.5%;\r\n    top: 870px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p19[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 78.5%;\r\n    top: 1252px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p20[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 64.5%;\r\n    top: 1252px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p21[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 50.5%;\r\n    top: 1252px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p22[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 37.5%;\r\n    top: 1252px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p23[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 23.5%;\r\n    top: 1252px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p24[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 9.5%;\r\n    top: 1252px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p25[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 83.5%;\r\n    top: 1698px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p26[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 70.5%;\r\n    top: 1698px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p27[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 56.5%;\r\n    top: 1698px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p28[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 42.5%;\r\n    top: 1698px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p29[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 29.5%;\r\n    top: 1698px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p30[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 15.5%;\r\n    top: 1698px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p31[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 78.5%;\r\n    top: 2072px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p32[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 64.5%;\r\n    top: 2072px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p33[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 51.5%;\r\n    top: 2072px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p34[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 37.5%;\r\n    top: 2072px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p35[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 23.5%;\r\n    top: 2072px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p36[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 9.5%;\r\n    top: 2072px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p37[_ngcontent-%COMP%]{\r\n    \r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 83.5%;\r\n    top: 2511px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n\r\n}\r\n.p38[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 69.5%;\r\n    top: 2511px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p39[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 56.5%;\r\n    top: 2511px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n\r\n}\r\n.p40[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 42.5%;\r\n    top: 2511px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p41[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 29.5%;\r\n    top: 2511px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p42[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 15.5%;\r\n    top: 2511px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p43[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 78.5%;\r\n    top: 2884px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p44[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 64.5%;\r\n    top: 2884px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p45[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 50.5%;\r\n    top: 2884px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p46[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 36.5%;\r\n    top: 2884px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n\r\n}\r\n.p47[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 23.5%;\r\n    top: 2884px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n\r\n}\r\n.p48[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 9.5%;\r\n    top: 2884px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p49[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 83.5%;\r\n    top: 3317px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p50[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 70.5%;\r\n    top: 3317px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p51[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 56.5%;\r\n    top: 3317px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p52[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 42.5%;\r\n    top: 3317px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p53[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 29.5%;\r\n    top: 3317px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p54[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 16.5%;\r\n    top: 3317px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p55[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 78.5%;\r\n    top: 3693px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p56[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 64.5%;\r\n    top: 3693px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p57[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 51.5%;\r\n    top: 3693px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p58[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 37.5%;\r\n    top: 3693px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p59[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 23.5%;\r\n    top: 3693px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p60[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 9.5%;\r\n    top: 3693px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p61[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 83.5%;\r\n    top: 4131px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p62[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 69.5%;\r\n    top: 4131px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p63[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 56.5%;\r\n    top: 4131px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p64[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 42.5%;\r\n    top: 4131px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n\r\n}\r\n.p65[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 29.5%;\r\n    top: 4131px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p66[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 15.5%;\r\n    top: 4131px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p67[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 77.5%;\r\n    top: 4505px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p68[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 63.5%;\r\n    top: 4505px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p69[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 49.5%;\r\n    top: 4505px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p70[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 36.5%;\r\n    top: 4505px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p71[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 22.5%;\r\n    top: 4505px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\r\n.p72[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    background-color: transparent;\r\n    width: 11.5%;\r\n    height: 346px;\r\n    right: 9.5%;\r\n    top: 4504px;\r\n    \r\n    border: transparent;\r\n    outline: none;\r\n    z-index: 200;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zbWFsbHBob25lLWltZy9zbWFsbHBob25lLWltZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksV0FBVztJQUNYLG9CQUFvQjtJQUNwQiwyQkFBMkIsRUFBRSxxQkFBcUI7SUFDbEQsNEJBQTRCLEVBQUUsNEJBQTRCO0lBQzFELHNCQUFzQixFQUFFLDhEQUE4RDtBQUMxRjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixTQUFTOztJQUVULG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixTQUFTOztJQUVULG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixTQUFTOztJQUVULG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixTQUFTOztJQUVULG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixTQUFTOztJQUVULG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixTQUFTOztJQUVULG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFdBQVc7SUFDWCxVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixVQUFVOztJQUVWLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFdBQVc7SUFDWCxXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFdBQVc7SUFDWCxXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBOztJQUVJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7O0FBRWhCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZOztBQUVoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTs7QUFFaEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVzs7SUFFWCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7O0FBRWhCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsV0FBVztJQUNYLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsV0FBVztJQUNYLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7O0lBRVgsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixZQUFZOztBQUVoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFlBQVk7SUFDWixXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixZQUFZO0lBQ1osYUFBYTtJQUNiLFdBQVc7SUFDWCxXQUFXOztJQUVYLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsWUFBWTtBQUNoQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc21hbGxwaG9uZS1pbWcvc21hbGxwaG9uZS1pbWcuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluLWhvbWV7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIC8qIGhlaWdodDogNTAwMHB4OyAqL1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyOyAvKiBDZW50ZXIgdGhlIGltYWdlICovXHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0OyAvKiBEbyBub3QgcmVwZWF0IHRoZSBpbWFnZSAqL1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjsgLyogUmVzaXplIHRoZSBiYWNrZ3JvdW5kIGltYWdlIHRvIGNvdmVyIHRoZSBlbnRpcmUgY29udGFpbmVyICovXHJcbn1cclxuLnAxe1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDgzLjUlO1xyXG4gICAgdG9wOiA1OXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDJ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogNzAuNSU7XHJcbiAgICB0b3A6IDU5cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wM3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA1Ni41JTtcclxuICAgIHRvcDogNTlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA0e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDQyLjUlO1xyXG4gICAgdG9wOiA1OXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogMjkuNSU7XHJcbiAgICB0b3A6IDU5cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiAxNS41JTtcclxuICAgIHRvcDogNTlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA3e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDc4LjUlO1xyXG4gICAgdG9wOiA0MjlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA4e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDY0LjUlO1xyXG4gICAgdG9wOiA0MjlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA5e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDUwLjUlO1xyXG4gICAgdG9wOiA0MjlweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAxMHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiAzNi41JTtcclxuICAgIHRvcDogNDI5cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMTF7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogMjMuNSU7XHJcbiAgICB0b3A6IDQyOXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDEye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDkuNSU7XHJcbiAgICB0b3A6IDQyOXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDEze1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDgzLjUlO1xyXG4gICAgdG9wOiA4NzBweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAxNHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA3MC41JTtcclxuICAgIHRvcDogODcwcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMTV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogNTYuNSU7XHJcbiAgICB0b3A6IDg3MHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDE2e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDQyLjUlO1xyXG4gICAgdG9wOiA4NzBweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAxN3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiAyOS41JTtcclxuICAgIHRvcDogODcwcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMTh7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogMTUuNSU7XHJcbiAgICB0b3A6IDg3MHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDE5e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDc4LjUlO1xyXG4gICAgdG9wOiAxMjUycHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMjB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogNjQuNSU7XHJcbiAgICB0b3A6IDEyNTJweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAyMXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA1MC41JTtcclxuICAgIHRvcDogMTI1MnB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDIye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDM3LjUlO1xyXG4gICAgdG9wOiAxMjUycHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMjN7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogMjMuNSU7XHJcbiAgICB0b3A6IDEyNTJweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAyNHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA5LjUlO1xyXG4gICAgdG9wOiAxMjUycHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMjV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogODMuNSU7XHJcbiAgICB0b3A6IDE2OThweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAyNntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA3MC41JTtcclxuICAgIHRvcDogMTY5OHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDI3e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDU2LjUlO1xyXG4gICAgdG9wOiAxNjk4cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMjh7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogNDIuNSU7XHJcbiAgICB0b3A6IDE2OThweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAyOXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiAyOS41JTtcclxuICAgIHRvcDogMTY5OHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDMwe1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDE1LjUlO1xyXG4gICAgdG9wOiAxNjk4cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMzF7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogNzguNSU7XHJcbiAgICB0b3A6IDIwNzJweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAzMntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA2NC41JTtcclxuICAgIHRvcDogMjA3MnB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDMze1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDUxLjUlO1xyXG4gICAgdG9wOiAyMDcycHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMzR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogMzcuNSU7XHJcbiAgICB0b3A6IDIwNzJweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAzNXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiAyMy41JTtcclxuICAgIHRvcDogMjA3MnB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDM2e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDkuNSU7XHJcbiAgICB0b3A6IDIwNzJweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnAzN3tcclxuICAgIFxyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDgzLjUlO1xyXG4gICAgdG9wOiAyNTExcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG5cclxufVxyXG4ucDM4e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDY5LjUlO1xyXG4gICAgdG9wOiAyNTExcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wMzl7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogNTYuNSU7XHJcbiAgICB0b3A6IDI1MTFweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcblxyXG59XHJcbi5wNDB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogNDIuNSU7XHJcbiAgICB0b3A6IDI1MTFweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA0MXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiAyOS41JTtcclxuICAgIHRvcDogMjUxMXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDQye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDE1LjUlO1xyXG4gICAgdG9wOiAyNTExcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNDN7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogNzguNSU7XHJcbiAgICB0b3A6IDI4ODRweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA0NHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA2NC41JTtcclxuICAgIHRvcDogMjg4NHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDQ1e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDUwLjUlO1xyXG4gICAgdG9wOiAyODg0cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNDZ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogMzYuNSU7XHJcbiAgICB0b3A6IDI4ODRweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcblxyXG59XHJcbi5wNDd7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogMjMuNSU7XHJcbiAgICB0b3A6IDI4ODRweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcblxyXG59XHJcbi5wNDh7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogOS41JTtcclxuICAgIHRvcDogMjg4NHB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDQ5e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDgzLjUlO1xyXG4gICAgdG9wOiAzMzE3cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNTB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogNzAuNSU7XHJcbiAgICB0b3A6IDMzMTdweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA1MXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA1Ni41JTtcclxuICAgIHRvcDogMzMxN3B4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDUye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDQyLjUlO1xyXG4gICAgdG9wOiAzMzE3cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNTN7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogMjkuNSU7XHJcbiAgICB0b3A6IDMzMTdweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA1NHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiAxNi41JTtcclxuICAgIHRvcDogMzMxN3B4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDU1e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDc4LjUlO1xyXG4gICAgdG9wOiAzNjkzcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNTZ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogNjQuNSU7XHJcbiAgICB0b3A6IDM2OTNweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA1N3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA1MS41JTtcclxuICAgIHRvcDogMzY5M3B4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDU4e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDM3LjUlO1xyXG4gICAgdG9wOiAzNjkzcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNTl7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogMjMuNSU7XHJcbiAgICB0b3A6IDM2OTNweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA2MHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA5LjUlO1xyXG4gICAgdG9wOiAzNjkzcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNjF7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogODMuNSU7XHJcbiAgICB0b3A6IDQxMzFweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA2MntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA2OS41JTtcclxuICAgIHRvcDogNDEzMXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDYze1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDU2LjUlO1xyXG4gICAgdG9wOiA0MTMxcHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNjR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogNDIuNSU7XHJcbiAgICB0b3A6IDQxMzFweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcblxyXG59XHJcbi5wNjV7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogMjkuNSU7XHJcbiAgICB0b3A6IDQxMzFweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA2NntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiAxNS41JTtcclxuICAgIHRvcDogNDEzMXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDY3e1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDc3LjUlO1xyXG4gICAgdG9wOiA0NTA1cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNjh7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogNjMuNSU7XHJcbiAgICB0b3A6IDQ1MDVweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA2OXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA0OS41JTtcclxuICAgIHRvcDogNDUwNXB4O1xyXG4gICAgXHJcbiAgICBib3JkZXI6IHRyYW5zcGFyZW50O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHotaW5kZXg6IDIwMDtcclxufVxyXG4ucDcwe1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICB3aWR0aDogMTEuNSU7XHJcbiAgICBoZWlnaHQ6IDM0NnB4O1xyXG4gICAgcmlnaHQ6IDM2LjUlO1xyXG4gICAgdG9wOiA0NTA1cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59XHJcbi5wNzF7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIHdpZHRoOiAxMS41JTtcclxuICAgIGhlaWdodDogMzQ2cHg7XHJcbiAgICByaWdodDogMjIuNSU7XHJcbiAgICB0b3A6IDQ1MDVweDtcclxuICAgIFxyXG4gICAgYm9yZGVyOiB0cmFuc3BhcmVudDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB6LWluZGV4OiAyMDA7XHJcbn1cclxuLnA3MntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgd2lkdGg6IDExLjUlO1xyXG4gICAgaGVpZ2h0OiAzNDZweDtcclxuICAgIHJpZ2h0OiA5LjUlO1xyXG4gICAgdG9wOiA0NTA0cHg7XHJcbiAgICBcclxuICAgIGJvcmRlcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgei1pbmRleDogMjAwO1xyXG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SmallphoneImgComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-smallphone-img',
                templateUrl: './smallphone-img.component.html',
                styleUrls: ['./smallphone-img.component.css']
            }]
    }], function () { return [{ type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] }, { type: _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_3__["LoadingBarService"] }]; }, { init: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ["init"]
        }] }); })();


/***/ }),

/***/ "./src/app/services/idioma.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/idioma.service.ts ***!
  \********************************************/
/*! exports provided: IdiomasLanding */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdiomasLanding", function() { return IdiomasLanding; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class IdiomasLanding {
    constructor() {
        this.idioma = "english";
        this.img = {
            english: "./assets/home/english.jpg",
            espanol: "./assets/home/espanol.jpg",
            aleman: "./assets/home/aleman.jpg",
            italiano: "./assets/home/italiano.jpg",
            portugues: "./assets/home/portugues.jpg",
            frances: "./assets/home/frances.jpg"
        };
    }
}
IdiomasLanding.ɵfac = function IdiomasLanding_Factory(t) { return new (t || IdiomasLanding)(); };
IdiomasLanding.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: IdiomasLanding, factory: IdiomasLanding.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](IdiomasLanding, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Proyecto kamila 10\Desktop\pk\halsey\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map