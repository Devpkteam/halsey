import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IdiomasLanding {
  constructor() { }
  idioma:string="english";
  img = {
    english:"./assets/home/english.jpg",
    espanol:"./assets/home/espanol.jpg",
    aleman:"./assets/home/aleman.jpg",
    italiano:"./assets/home/italiano.jpg",
    portugues:"./assets/home/portugues.jpg",
    frances:"./assets/home/frances.jpg"
  }
  
}
