
import { Component, ViewChild, ElementRef, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';
import { DescargaComponent } from '../modal/descarga/descarga.component';
import {LoadingBarService} from '@ngx-loading-bar/core'

@Component({
  selector: 'app-smallphone-img',
  templateUrl: './smallphone-img.component.html',
  styleUrls: ['./smallphone-img.component.css']
})
export class SmallphoneImgComponent {
  @ViewChild("init") init:ElementRef
  constructor(
    public dialog: MatDialog,
    public loadingBarService:LoadingBarService
  ) {
    const muestraWidth = ()=>{
      setTimeout(()=>{
        const x = (<HTMLElement>document.getElementById("background-img")).getBoundingClientRect().width
        console.log(x)
        const elements = ['p1','p2','p3','p4','p5','p6']
        const elements2 = ['p7','p8','p9','p10','p11','p12']
        const elements3 = ['p13','p14','p15','p16','p17','p18']
        const elements4 = ['p19','p20','p21','p22','p23','p24']
        const elements5 = ['p25','p26','p27','p28','p29','p30']
        const elements6 = ['p31','p32','p33','p34','p35','p36']
        const elements7 = ['p37','p38','p39','p40','p41','p42']
        const elements8 = ['p43','p44','p45','p46','p47','p48']
        const elements9 = ['p49','p50','p51','p52','p53','p54']
        const elements10 = ['p55','p56','p57','p58','p59','p60']
        const elements11 = ['p61','p62','p63','p64','p65','p66']
        const elements12 = ['p67','p68','p69','p70','p71','p72']

        for(let i in elements){
          const element = document.getElementById(elements[i])
          element.style.top = `${35*x/1108}px`
          element.style.height = `${234*x/1108}px`
        }
        for(let i in elements2){
          const element = document.getElementById(elements2[i])
          element.style.top = `${273*x/1108}px`
          element.style.height = `${234*x/1108}px`
        }
        for(let i in elements3){
          const element = document.getElementById(elements3[i])
          element.style.top = `${555*x/1108}px`
          element.style.height = `${234*x/1108}px`
        }
        for(let i in elements4){
          const element = document.getElementById(elements4[i])
          element.style.top = `${802*x/1108}px`
          element.style.height = `${234*x/1108}px`
        }
        for(let i in elements5){
          const element = document.getElementById(elements5[i])
          element.style.top = `${1089*x/1108}px`
          element.style.height = `${234*x/1108}px`
        }
        // const elements= ['veridiomas','donacion','contacto']
          for(let i in elements6){
            const element = document.getElementById(elements6[i])
            element.style.top = `${1329*x/1108}px`
            element.style.height = `${234*x/1108}px`
          }
          for(let i in elements7){
            const element = document.getElementById(elements7[i])
            element.style.top = `${1610*x/1108}px`
            element.style.height = `${234*x/1108}px`
          }
          for(let i in elements8){
            const element = document.getElementById(elements8[i])
            element.style.top = `${1852*x/1108}px`
            element.style.height = `${234*x/1108}px`
          }
          for(let i in elements9){
            const element = document.getElementById(elements9[i])
            element.style.top = `${2130*x/1108}px`
            element.style.height = `${234*x/1108}px`
          }
          for(let i in elements10){
            const element = document.getElementById(elements10[i])
            element.style.top = `${2370*x/1108}px`
            element.style.height = `${234*x/1108}px`
          }
          for(let i in elements11){
            const element = document.getElementById(elements11[i])
            element.style.top = `${2651*x/1108}px`
            element.style.height = `${234*x/1108}px`
          }
          for(let i in elements12){
            const element = document.getElementById(elements12[i])
            element.style.top = `${2892*x/1108}px`
            element.style.height = `${234*x/1108}px`
          }




        // for(let i in elements){
        //   const element = document.getElementById(elements[i])
        //   element.style.top = `${320*x/1108}px`
        // }
        // const element = document.getElementById('desktopImg')
        // element.style.top = `${1000*x/1662}px`
        // const element2 = document.getElementById('phoneImg')
        // element2.style.top = `${1959*x/1662}px`
        // const element3 = document.getElementById('smallphoneImg')
        // element3.style.top = `${2612*x/1662}px`
        // const element4 = document.getElementById('banderas')
        // element4.style.height = `${644*x/1662}px`
        muestraWidth()
      }, 400);
    }
    muestraWidth()
    setTimeout(()=>{
      this.init.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
    },500)
  }
  openDialog(link): void {
    console.log(link)
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '45%',
      data: link,
      position: {top: '10px'}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }
  doSomething(){
    // const dialogRef = this.dialog.open(DescargaComponent, {
    //   width: '40%'
    // });
    this.loadingBarService.complete()
  }

}
