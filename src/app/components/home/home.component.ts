import { Component, OnInit,  } from '@angular/core';
import { IdiomasLanding } from '../../services/idioma.service';
import { Router } from '@angular/router'
import { DescargaComponent } from '../modal/descarga/descarga.component';
import {MatDialog } from '@angular/material/dialog';
import { LoadingBarService } from '@ngx-loading-bar/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // @ViewChild("img",{static:false}) background_img: ElementRef;

  background:string;
  idiomasMostrar:boolean=false;
  constructor(
    private idiomas:IdiomasLanding,
    private router:Router,
    public dialog: MatDialog,
    private loadingBarService: LoadingBarService
  ) { 
    this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
    this.loadingBarService.start()
  }

  ngOnInit(): void {
    const muestraWidth = ()=>{
      setTimeout(()=>{
        const x = (<HTMLElement>document.getElementById("background-img")).getBoundingClientRect().width
        console.log(x)
        const elements= ['veridiomas','donacion','contacto']
        for(let i in elements){
          const element = document.getElementById(elements[i])
          element.style.top = `${320*x/1108}px`
        }
        const element = document.getElementById('desktopImg')
        element.style.top = `${1000*x/1662}px`
        const element2 = document.getElementById('phoneImg')
        element2.style.top = `${1959*x/1662}px`
        const element3 = document.getElementById('smallphoneImg')
        element3.style.top = `${2612*x/1662}px`
        const element4 = document.getElementById('banderas')
        element4.style.height = `${351*x/1022}px`
        element4.style.top = `${48*x/1022}px`
        const element5 = document.getElementById('bandera')
        element5.style.width = `${90*x/1662}px`
        element5.style.marginLeft = `${33*x/1662}px`
        const banderas = ['bandera1','bandera2','bandera3','bandera4','bandera5']
        for(let i in banderas){
          const e =document.getElementById(banderas[i])
          e.style.width = `${90*x/1662}px`
          e.style.marginLeft = `${33*x/1662}px`
        }
        muestraWidth()
      }, 400);
    }
    muestraWidth()
  }

  veridiomas(){this.idiomasMostrar = !this.idiomasMostrar}
  donacion(){
    this.router.navigate(['/cabina-pago']);
    this.loadingBarService.start();
  }
  contacto(){
    this.router.navigate(['/contacto']);
    this.loadingBarService.start();
  }

  setingles(){ 
    // this.idiomas.idioma==="english"?null:this.loadingBarService.start();
    const dialogRef = this.dialog.open(DescargaComponent, {
      width: '40%'
    });
    this.idiomas.idioma = "english"; 
    this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
  }
  setespanol(){
    // this.idiomas.idioma==="espanol"?null:this.loadingBarService.start();
    const dialogRef = this.dialog.open(DescargaComponent, {
      width: '40%'
    });
    this.idiomas.idioma = "espanol"; this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
    }
  setaleman(){
    // this.idiomas.idioma==="aleman"?null:this.loadingBarService.start();
    const dialogRef = this.dialog.open(DescargaComponent, {
      width: '40%'
    });
     this.idiomas.idioma = "aleman"; this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
    }
  setitaliano(){ 
    // this.idiomas.idioma==="italiano"?null:this.loadingBarService.start();
    const dialogRef = this.dialog.open(DescargaComponent, {
      width: '40%'
    });
    this.idiomas.idioma = "italiano"; this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
  }
  setportugues(){
    // this.idiomas.idioma==="portugues"?null:this.loadingBarService.start();
    const dialogRef = this.dialog.open(DescargaComponent, {
      width: '40%'
    });
     this.idiomas.idioma = "portugues"; this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
    }
  setfrances(){ 
   /* this.idiomas.idioma==="frances"?null:this.loadingBarService.start(); */
   const dialogRef = this.dialog.open(DescargaComponent, {
      width: '40%'
    });
    this.idiomas.idioma = "frances"; this.background = `./assets/home/${this.idiomas.idioma}.jpg`;
  }

  desktopImg(){
    this.router.navigate(['/wallpers-escritorio'])
    this.loadingBarService.start()
  }
  phoneImg(){
    this.router.navigate(['/wallpers-telefono'])
    this.loadingBarService.start()
  }
  smallphoneImg(){
    this.router.navigate(['/wallpers-telefono-pequeno'])
    this.loadingBarService.start()
  }
  doSomething(){
    // const dialogRef = this.dialog.open(DescargaComponent, {
    //   width: '40%'
    // });
    this.loadingBarService.complete()
  }
}
