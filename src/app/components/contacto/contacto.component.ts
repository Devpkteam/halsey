import { Component, OnInit } from '@angular/core';
import { IdiomasLanding } from '../../services/idioma.service';
import { DescargaComponent } from '../modal/descarga/descarga.component';
import {MatDialog } from '@angular/material/dialog';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {
  background:string;
  constructor(
    private idiomas:IdiomasLanding,
    public dialog: MatDialog,
    public loadingBarService: LoadingBarService
  ) { 
    this.background = `assets/contacto/${idiomas.idioma}.jpg`
  }
  ngOnInit(): void {
  }
  doSomething(){
    // const dialogRef = this.dialog.open(DescargaComponent, {
    //   width: '40%'
    // });
    this.loadingBarService.complete();
  }
}
