import { Component, ViewChild, ElementRef, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';
import { DescargaComponent } from '../modal/descarga/descarga.component';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'app-phone-img',
  templateUrl: './phone-img.component.html',
  styleUrls: ['./phone-img.component.css']
})
export class PhoneImgComponent  {
  @ViewChild("init") init:ElementRef
  constructor(
    public dialog: MatDialog,
    public loadingBarService:LoadingBarService
  ) {
    const muestraWidth = ()=>{
      setTimeout(()=>{
        const x = (<HTMLElement>document.getElementById("background-img")).getBoundingClientRect().width
        console.log(x)

        const elements = ['p1','p2','p3','p4','p5','p6']
        const elements2 = ['p7','p8','p9','p10','p11','p12']
        const elements3 = ['p13','p14','p15','p16','p17','p18']
        const elements4 = ['p19','p20','p21','p22','p23','p24']
        const elements5 = ['p25','p26','p27','p28','p29','p30']
        const elements6 = ['p31','p32','p33','p34','p35','p36']
        const elements7 = ['p37','p38','p39','p40','p41','p42']
        const elements8 = ['p43','p44','p45','p46','p47','p48']
        



        for(let i in elements){
          const element = document.getElementById(elements[i])
          element.style.top = `${26*x/1108}px`
          element.style.height = `${231*x/1108}px`
        }
        for(let i in elements2){
          const element = document.getElementById(elements2[i])
          element.style.top = `${269*x/1108}px`
          element.style.height = `${231*x/1108}px`
        }
        for(let i in elements3){
          const element = document.getElementById(elements3[i])
          element.style.top = `${542*x/1108}px`
          element.style.height = `${231*x/1108}px`
        }
        for(let i in elements4){
          const element = document.getElementById(elements4[i])
          element.style.top = `${783*x/1108}px`
          element.style.height = `${231*x/1108}px`
        }
        for(let i in elements5){
          const element = document.getElementById(elements5[i])
          element.style.top = `${1057*x/1108}px`
          element.style.height = `${231*x/1108}px`
        }
        for(let i in elements6){
          const element = document.getElementById(elements6[i])
          element.style.top = `${1303*x/1108}px`
          element.style.height = `${231*x/1108}px`
        }
        for(let i in elements7){
          const element = document.getElementById(elements7[i])
          element.style.top = `${1584*x/1108}px`
          element.style.height = `${231*x/1108}px`
       
        }
        for(let i in elements8){
          const element = document.getElementById(elements8[i])
          element.style.top = `${1833*x/1108}px`
          element.style.height = `${231*x/1108}px`
       
        }
        // const elements= ['veridiomas','donacion','contacto']
        // for(let i in elements){
        //   const element = document.getElementById(elements[i])
        //   element.style.top = `${320*x/1108}px`
        // }
        
        muestraWidth()
      }, 400);
    }
    muestraWidth()
    setTimeout(()=>{
      this.init.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
    },500)
  }
  openDialog(link): void {
    console.log(link)
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '45%',
      data: link,
      position: {top: '10px'}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }
  doSomething(){
    // const dialogRef = this.dialog.open(DescargaComponent, {
    //   width: '40%'
    // });
    this.loadingBarService.complete();
  }

}
