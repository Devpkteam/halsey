import { Component, OnInit } from '@angular/core';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { IdiomasLanding } from '../../services/idioma.service';
import { ModalComponent } from '../modal/modal.component';
import {MatDialog} from '@angular/material/dialog';



@Component({
  selector: 'app-exclusivo',
  templateUrl: './exclusivo.component.html',
  styleUrls: ['./exclusivo.component.css']
})
export class ExclusivoComponent implements OnInit {
  background:string;
  constructor(
    public loadingBarService: LoadingBarService,
    private idiomas:IdiomasLanding,
    public dialog: MatDialog,

  ) {
    this.background = `assets/exclusivo/list.jpg`
    const muestraWidth = ()=>{
      setTimeout(()=>{
        const x = (<HTMLElement>document.getElementById("background-img")).getBoundingClientRect().width
        console.log(x)
           const elements= ['p1','p2','p3']
          const elements2 = ['p4','p5','p6']
        const elements3 = ['p7','p8','p9']
        

        for(let i in elements){
          const element = document.getElementById(elements[i])
          element.style.top = `${33*x/1014}px`
          element.style.height = `${146*x/1014}px`
        }
        for(let i in elements2){
          const element = document.getElementById(elements2[i])
          element.style.top = `${189*x/1014}px`
          element.style.height = `${146*x/1014}px`
        }
        for(let i in elements3){
          const element = document.getElementById(elements3[i])
          element.style.top = `${345*x/1014}px`
          element.style.height = `${146*x/1014}px`
        }


        // for(let i in elements){
        //   const element = document.getElementById(elements[i])
        //   element.style.top = `${320*x/1108}px`
        // }
        // const element = document.getElementById('desktopImg')
        // element.style.top = `${1000*x/1662}px`
        // const element2 = document.getElementById('phoneImg')
        // element2.style.top = `${1959*x/1662}px`
        // const element3 = document.getElementById('smallphoneImg')
        // element3.style.top = `${2612*x/1662}px`
        // const element4 = document.getElementById('banderas')
        // element4.style.height = `${644*x/1662}px`
        muestraWidth()
      }, 400);
    }
    muestraWidth()
  }

  ngOnInit(): void {
  }
  openDialog(link): void {
    console.log(link)
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '45%',
      data: link,
      position: {top: '10px'}

    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  doSomething(){
    this.loadingBarService.complete()
  }

}
