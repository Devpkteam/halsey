import { Component, ViewChild, ElementRef, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';
import { DescargaComponent,} from '../modal/descarga/descarga.component';
import { LoadingBarService } from '@ngx-loading-bar/core';


@Component({
  selector: 'app-desktop-img',
  templateUrl: './desktop-img.component.html',
  styleUrls: ['./desktop-img.component.css']
})
export class DesktopImgComponent implements OnInit{
  @ViewChild("init") init:ElementRef
  constructor(
    public dialog: MatDialog,
    private loadingBarService: LoadingBarService
  ) {
    const muestraWidth = ()=>{
      setTimeout(()=>{
        const x = (<HTMLElement>document.getElementById("background-img")).getBoundingClientRect().width
        console.log(x)
        const elements= ['p1','p2','p3']
        const elements2= ['p4','p5','p6']
        const elements3= ['p7','p8','p9']
        const elements4= ['p10','p11','p12']
        const elements5= ['p13','p14','p15']
        const elements6= ['p16','p17','p18']
        const elements7= ['p19','p20','p21']
        const elements8= ['p22','p23','p24']
        const elements9= ['p25','p26','p27']
        const elements10= ['p28','p29','p30']
        const elements11= ['p31','p32','p33']
        const elements12= ['p34','p35','p36']
        const elements13= ['p37','p38','p39']
        const elements14= ['p40','p41','p42']
        const elements15= ['p43','p44','p45']
       
      
        for(let i in elements){
          const element = document.getElementById(elements[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${28*x/923}px`
        }
        for(let i in elements2){
          const element = document.getElementById(elements2[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${166*x/923}px`
        }
        for(let i in elements3){
          const element = document.getElementById(elements3[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${307*x/923}px`
        }
        for(let i in elements4){
          const element = document.getElementById(elements4[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${475*x/923}px`
        }
        for(let i in elements5){
          const element = document.getElementById(elements5[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${613*x/923}px`
        }
        for(let i in elements6){
          const element = document.getElementById(elements6[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${760*x/923}px`
        }
        for(let i in elements7){
          const element = document.getElementById(elements7[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${928*x/923}px`
        }
        for(let i in elements8){
          const element = document.getElementById(elements8[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${1069*x/923}px`
        }
        for(let i in elements9){
          const element = document.getElementById(elements9[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${1206*x/923}px`
        }
        for(let i in elements10){
          const element = document.getElementById(elements10[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${1371*x/923}px`
        }
        for(let i in elements11){
          const element = document.getElementById(elements11[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${1510*x/923}px`
        }
        for(let i in elements12){
          const element = document.getElementById(elements12[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${1652*x/923}px`
        }
        for(let i in elements13){
          const element = document.getElementById(elements13[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${1824*x/923}px`
        }
        for(let i in elements14){
          const element = document.getElementById(elements14[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${1965*x/923}px`
        }
        for(let i in elements15){
          const element = document.getElementById(elements15[i])
          element.style.height = `${126*x/923}px`
          element.style.top = `${2102*x/923}px`
        }   
        
        // const element = document.getElementById('desktopImg')
        // element.style.top = `${1000*x/1662}px`
        // const element2 = document.getElementById('phoneImg')
        // element2.style.top = `${1959*x/1662}px`
        // const element3 = document.getElementById('smallphoneImg')
        // element3.style.top = `${2612*x/1662}px`
        // const element4 = document.getElementById('banderas')
        // element4.style.height = `${644*x/1662}px`
        muestraWidth()
      }, 400);
    }
    muestraWidth()
    // this.init.scrollIntoView();
    
  }
  ngOnInit(){
    setTimeout(()=>{
      this.init.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
    },500)
  }
  openDialog(link): void {
    console.log(link)
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '45%',
      data: link,
      position: {top: '10px'}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }
  doSomething(){
    // const dialogRef = this.dialog.open(DescargaComponent, {
    //   width: '40%'
    // });
    this.loadingBarService.complete()
  }


}
