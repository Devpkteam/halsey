import { Component } from '@angular/core';
import { IdiomasLanding } from '../../services/idioma.service';
import {MatDialog} from '@angular/material/dialog';
import { ModalPagoComponent } from './modal-pago/modal-pago.component';
import { DescargaComponent } from '../modal/descarga/descarga.component';
import { LoadingBarService} from '@ngx-loading-bar/core'
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { IPayPalConfig, ICreateOrderRequest } from 'ngx-paypal';

@Component({
  selector: 'app-plataforma-pago',
  templateUrl: './plataforma-pago.component.html',
  styleUrls: ['./plataforma-pago.component.css']
})
export class PlataformaPagoComponent  {
  oculter=true
  background2:string="./assets/formulario-pago/"
  form:FormGroup;
  payPalConfig?: IPayPalConfig;
  amountDonate:string;
  seeForm:boolean=true;
  materialExclusivo:boolean=false;
  background:string;
  constructor(
    private idiomas:IdiomasLanding,
    private fb: FormBuilder,
    public dialog: MatDialog,
    public loadingBarService:LoadingBarService,
  ) {
    this.form = this.fb.group({
      correo: ['', Validators.required ],
      monto: ['', Validators.required ],
    });
    this.background = `assets/cabina-pago/${idiomas.idioma}.jpg`;
    this.background2= `${this.background2}${idiomas.idioma}.png`
    const muestraWidth = ()=>{
      setTimeout(()=>{
        const x = (<HTMLElement>document.getElementById("background-img")).getBoundingClientRect().width
        console.log(x)
        const modal = document.getElementById("background2")
        modal.style.top= `${114*x/1125}px`
        modal.style.left= `${600*x/1125}px`

        const email = document.getElementById("email")
        email.style.top= `${147*x/1125}px`
        email.style.left= `${619*x/1125}px`
        email.style.width= `${302*x/1125}px`
        email.style.height= `${23*x/1125}px`
        const monto = document.getElementById("monto")
        monto.style.top= `${232*x/1125}px`
        monto.style.left= `${648*x/1125}px`
        monto.style.width= `${273*x/1125}px`
        monto.style.height= `${23*x/1125}px`
        const donar = document.getElementById("donara")
        donar.style.top= `${284*x/1125}px`
        donar.style.left= `${618*x/1125}px`
        donar.style.width= `${210*x/1125}px`
        donar.style.left= `${620*x/1125}px`
        donar.style.height= `${28*x/1125}px`
        // const element = document.getElementById('desktopImg')
        // element.style.top = `${1000*x/1662}px`
        // const element2 = document.getElementById('phoneImg')
        // element2.style.top = `${1959*x/1662}px`
        // const element3 = document.getElementById('smallphoneImg')
        // element3.style.top = `${2612*x/1662}px`
        // const element4 = document.getElementById('banderas')
        // element4.style.height = `${644*x/1662}px`
        muestraWidth()
        
      }, 400);
      
    }
    muestraWidth()
    // const dialogRef = this.dialog.open(ModalPagoComponent, {
    //   width: '40%',
    //   panelClass: 'modal-pago',
    //   disableClose: true,
    //   position: {top: '80px', left:'55%'}
    // });
  }
  pagar(){
    const dialogRef = this.dialog.open(ModalPagoComponent, {
      width: '30%',
      position: {top: '160px', left:'500px'}
    });
    // ,{ top: '50px', left: '50px' }
  }
  doSomething(){
    // const dialogRef = this.dialog.open(DescargaComponent, {
    //   width: '40%'
    // });
    this.loadingBarService.complete();
  }


  donar(){
    console.log(this.form.value.monto)
    if(this.form.value.monto !== "" && !isNaN(this.form.value.monto)){
      localStorage.setItem("donacion",this.form.value.monto)
      const dialogRef = this.dialog.open(ModalPagoComponent, {
          width: '35%',
          panelClass: 'modal-pago',
          disableClose: true,
          position: {top: '80px', left:'55%'}
        });
  
        dialogRef.afterClosed().subscribe(()=>{
          this.oculter=true
        })
        this.oculter=false
    }else{
      alert("El monto ingresado no es valido")
    }
    // console.log(this.form.value);
    // this.seeForm=false;
    // this.amountDonate= this.form.value.monto
    // this.initConfig();
  }
  private initConfig(): void {
    this.payPalConfig = {
    currency: 'USD',
    clientId: 'sb', // ID OF THE PAYPAL ACCOUNT OF THE CLIENT
    createOrderOnClient: (data) => <ICreateOrderRequest>{
      intent: 'CAPTURE',
      purchase_units: [
        {
          amount: {
            currency_code: 'USD',
            value: this.amountDonate,
            breakdown: {
              item_total: {
                currency_code: 'USD',
                value: this.amountDonate
              }
            }
          },
          items: [
            {
              name: 'Donación Digital Halsey',
              quantity: '1',
              category: 'DIGITAL_GOODS',
              unit_amount: {
                currency_code: 'USD',
                value: this.amountDonate,
              },
            }
          ]
        }
      ]
    },
    advanced: {
      commit: 'true'
    },
    style: {
      label: 'paypal',
      layout: 'vertical',
    },
    onApprove: (data, actions) => {
      console.log('onApprove - transaction was approved, but not authorized', data, actions);
      actions.order.get().then(details => {
        console.log('onApprove - you can get full order details inside onApprove: ', details);
        this.materialExclusivo = true;
      });
    },
    onClientAuthorization: (data) => {
      console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
    },
    onCancel: (data, actions) => {
      console.log('OnCancel', data, actions);
    },
    onError: err => {
      console.log('OnError', err);
    },
    onClick: (data, actions) => {
      console.log('onClick', data, actions);
    },
  };
  }
}
