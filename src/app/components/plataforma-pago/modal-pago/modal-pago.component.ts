import { Component } from '@angular/core';
import { IPayPalConfig, ICreateOrderRequest } from 'ngx-paypal';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {IdiomasLanding} from '../../../services/idioma.service'
import { LoadingBarService} from '@ngx-loading-bar/core'


@Component({
  selector: 'app-modal-pago',
  templateUrl: './modal-pago.component.html',
  styleUrls: ['./modal-pago.component.css']
})
export class ModalPagoComponent  {
  background:string="./assets/formulario-pago/"
  form:FormGroup;
  payPalConfig?: IPayPalConfig;
  amountDonate:string=localStorage.getItem("donacion");
  seeForm:boolean=false;
  materialExclusivo:boolean=false;
  constructor(
    private fb: FormBuilder,
    private idioma: IdiomasLanding,
    public loadingBarService: LoadingBarService,
    ) {
    this.form = this.fb.group({
      correo: ['', Validators.required ],
      monto: ['', Validators.required ],
    });

    this.initConfig();
    this.background= `${this.background}${idioma.idioma}.png`
   
  }
  donar(){
    console.log(this.form.value);
    this.seeForm=false;
    this.amountDonate= this.form.value.monto
    
  }
  private initConfig(): void {
    this.payPalConfig = {
    currency: 'USD', // 7PK4JD8K9WKSS
    clientId: 'sb', // ID OF THE PAYPAL ACCOUNT OF THE CLIENT
    createOrderOnClient: (data) => <ICreateOrderRequest>{
      intent: 'CAPTURE',
      purchase_units: [
        {
          amount: {
            currency_code: 'USD',
            value: this.amountDonate,
            breakdown: {
              item_total: {
                currency_code: 'USD',
                value: this.amountDonate
              }
            }
          },
          items: [
            {
              name: 'Donación Digital Halsey',
              quantity: '1',
              category: 'DIGITAL_GOODS',
              unit_amount: {
                currency_code: 'USD',
                value: this.amountDonate,
              },
            }
          ]
        }
      ]
    },
    advanced: {
      commit: 'true'
    },
    style: {
      label: 'paypal',
      layout: 'vertical',
    },
    onApprove: (data, actions) => {
      console.log('onApprove - transaction was approved, but not authorized', data, actions);
      actions.order.get().then(details => {
        console.log('onApprove - you can get full order details inside onApprove: ', details);
        this.materialExclusivo = true;
      });
    },
    onClientAuthorization: (data) => {
      console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
    },
    onCancel: (data, actions) => {
      console.log('OnCancel', data, actions);
    },
    onError: err => {
      console.log('OnError', err);
    },
    onClick: (data, actions) => {
      console.log('onClick', data, actions);
    },
  };
  }
}
