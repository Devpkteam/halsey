import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { DescargaComponent } from './descarga/descarga.component';
import {IdiomasLanding} from '../../services/idioma.service';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent  {
  background:string='assets/popup/english.png';
  title:string="GO TO SITE START";
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string,
    public idioma: IdiomasLanding,
  ) {
    const muestraWidth = ()=>{
      setTimeout(()=>{
        const x = (<HTMLElement>document.getElementById("background-img")).getBoundingClientRect().width
        console.log(x)
        const elements= ['link','close','inicio']
        const element = document.getElementById(elements[0])
        element.style.top = `${373.573*x/1087}px`
        element.style.height = `${29*x/1087}px`
        const element2 = document.getElementById(elements[1])
        element2.style.top = `${419.365*x/1087}px`
        element2.style.height = `${29*x/1087}px`
        const element3 = document.getElementById(elements[2])
        element3.style.top = `${464.293*x/1087}px`
        element3.style.height = `${29*x/1087}px`
        muestraWidth()
      }, 400);
    }
    muestraWidth()
    console.log(this.data)
    this.background=`assets/popup/${this.idioma.idioma}.png`
    switch(this.idioma.idioma){
      case "english":
        this.title = `GO TO SITE START`;
        break;
        
      case "espanol":
        this.title = `IR AL INICIO DEL PORTAL`;
        break;
        
      case "aleman":
        this.title = `GEHE ZUM START DER SITE`;
        break;
        
      case "italiano":
        this.title = ` VAI ALL’INIZIO DEL SITO`;
        break;
        
      case "portugues":
        this.title = `IR PARA O INÌCIO DO SITE`;
        break;
        
      case "frances":
        this.title = `ALLER AU DÈBUT DU SITE`;
        break;
        
    }
  }
  download(){
    const dialogRef = this.dialog.open(DescargaComponent, {
      width: '40%'
    });
  }
}
