import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-descarga',
  templateUrl: './descarga.component.html',
  styleUrls: ['./descarga.component.css']
})
export class DescargaComponent  {
  background:string='assets/figurin/1.png'
  background2:string='assets/figurin/2.png'
  boolean:boolean=true;
  repeticiones:number=5;
  constructor(private dialogRef:MatDialogRef<DescargaComponent>){
    this.cambiarimg()
    dialogRef.disableClose = true;
  }
  cambiarimg(){
    setTimeout(()=>{this.boolean = !this.boolean;this.cambiarimg()}, 1000);
    this.repeticiones===0?this.dialogRef.close():this.repeticiones-=1;
  }
}
