import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { DesktopImgComponent } from './components/desktop-img/desktop-img.component';
import { PhoneImgComponent } from './components/phone-img/phone-img.component';
import { SmallphoneImgComponent } from './components/smallphone-img/smallphone-img.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { PlataformaPagoComponent } from './components/plataforma-pago/plataforma-pago.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalComponent } from './components/modal/modal.component';
import {MatDialogModule} from '@angular/material/dialog';
import { DescargaComponent } from './components/modal/descarga/descarga.component';
import { NgxPayPalModule } from 'ngx-paypal';
import { ModalPagoComponent } from './components/plataforma-pago/modal-pago/modal-pago.component';
import { ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { ExclusivoComponent } from './components/exclusivo/exclusivo.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DesktopImgComponent,
    PhoneImgComponent,
    SmallphoneImgComponent,
    ContactoComponent,
    PlataformaPagoComponent,
    ModalComponent,
    DescargaComponent,
    ModalPagoComponent,
    ExclusivoComponent,
  ],
  imports: [
    BrowserModule,
    MatFormFieldModule,
    MatButtonModule,
    LoadingBarModule,
    MatInputModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    NgxPayPalModule
  ],
  providers: [ModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
