import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { DesktopImgComponent } from './components/desktop-img/desktop-img.component';
import { PhoneImgComponent } from './components/phone-img/phone-img.component';
import { SmallphoneImgComponent } from './components/smallphone-img/smallphone-img.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { PlataformaPagoComponent } from './components/plataforma-pago/plataforma-pago.component';
import { ExclusivoComponent } from './components/exclusivo/exclusivo.component';


const routes: Routes = [
  {path:"", component:HomeComponent},
  {path:"wallpers-escritorio", component:DesktopImgComponent},
  {path:"wallpers-telefono", component:PhoneImgComponent},
  {path:"wallpers-telefono-pequeno", component:SmallphoneImgComponent},
  {path:"contacto", component:ContactoComponent},
  {path:"cabina-pago", component:PlataformaPagoComponent},
  {path:"exclusivo", component:ExclusivoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
